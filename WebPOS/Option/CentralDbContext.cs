﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using WebPOS.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using WebPOS.Models.ViewModels;
using System.Data.SqlClient;

namespace WebPOS.Option
{
    public class CentralDbContext : DbContext
    {
        //public CentralDbContext(DbContextOptions<CentralDbContext> options) : base(options) { }
        public DbSet<POSTerminalBank> POSTerminalBank { get; set; }
        public DbSet<UserRole> UserRole { get; set; }
        public DbSet<ItemInfo> ItemInfo { get; set; }
        public DbSet<Function> Function{ get; set; }
        public DbSet<LinkRolesMenus> LinkRolesMenus { get; set; }
        public DbSet<TransStatus> TransStatus { get; set; }
        public DbSet<SumInvoiceDetail> SumInvoiceDetail { get; set; }
        public DbSet<SumInvoiceInfo> SumInvoiceInfo { get; set; }
        public DbSet<Barcodes> Barcodes { get; set; }
        public DbSet<Branch> Branch { get; set; }
        public DbSet<CompanyInformation> CompanyInformation { get; set; }
        public DbSet<CpnVchBOMHeader> CpnVchBOMHeader { get; set; }
        public DbSet<CpnVchBOMLine> CpnVchBOMLine { get; set; }
        public DbSet<CouponHeader> CouponHeader { get; set; }       
        public DbSet<Currency> Currency { get; set; }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<OfferBenefits> OfferBenefits { get; set; }
        public DbSet<OfferBuy> OfferBuy { get; set; }
        public DbSet<OfferGet> OfferGet { get; set; }
        public DbSet<OfferHeader> OfferHeader { get; set; }
        public DbSet<OfferSite> OfferSite { get; set; }
        public DbSet<TransDiscountEntry> TransDiscountEntry { get; set; }
        public DbSet<TransHeader> TransHeader { get; set; }
        public DbSet<TransInfocodeEntry> TransInfocodeEntry { get; set; }
        public DbSet<TransLine> TransLine { get; set; }
        public DbSet<TransPaymentEntry> TransPaymentEntry { get; set; }
        public DbSet<POSTerminal> POSTerminal { get; set; }
        public DbSet<SalesPrice> SalesPrice { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<POSHotKeyItem> POSHotKeyItem { get; set; }
        public DbSet<StorePriceGroup> StorePriceGroup { get; set; }
        public DbSet<Store> Store { get; set; }
        public DbSet<POSDataSetup> POSDataSetup { get; set; }
        public DbSet<POSVATCode> POSVATCode { get; set; }
        public DbSet<ReasonCode> ReasonCode { get; set; }
        public DbSet<RetailSetup> RetailSetup { get; set; }
        public DbSet<Staff> Staff { get; set; }
        public DbSet<TenderType> TenderType { get; set; }
        public DbSet<TenderTypeSetup> TenderTypeSetup { get; set; }
        public DbSet<CustomerPriceGroup> CustomerPriceGroup { get; set; }
        public DbSet<Item> Item { get; set; }
        public DbSet<Infocode> Infocode { get; set; }
        public DbSet<InformationSubcode> InformationSubcode { get; set; }
        public DbSet<ItemEarnScale> ItemEarnScale { get; set; }
        public DbSet<SysMenu> SysMenu { get; set; }
        public DbSet<SysModules> SysModules { get; set; }
        public DbSet<TableSpecificInfocode> TableSpecificInfocode { get; set; }
        public DbSet<ItemMaxSalesQty> ItemMaxSalesQty { get; set; }
        public DbSet<LinkedItem> LinkedItem { get; set; }
        public DbSet<Role> Role { get; set; }
        internal Task LoginByUsernamePasswordMethodAsync(object username, object password)
        {
            throw new NotImplementedException();
        }

        public DbSet<ItemUnitOfMeasure> ItemUnitOfMeasure { get; set; }
        public DbSet<TransVoidHeader> TransVoidHeader { get; set; }
        public DbSet<TransVoidLine> TransVoidLine { get; set; }
        public DbSet<UnitOfMeasure> UnitOfMeasure { get; set; }
        public DbSet<POSDocumentNos> POSDocumentNos { get; set; }
        public DbSet<POSEndOfDay> POSEndOfDay { get; set; }
        public DbSet<POSEndOfShiftHeader> POSEndOfShiftHeader { get; set; }
        public DbSet<POSEndOfShiftLine> POSEndOfShiftLine { get; set; }
        public DbSet<POSSetupEndOfShift> POSSetupEndOfShift { get; set; }
        public DbSet<POSVersion> POSVersion { get; set; }
        public DbSet<SpecialOrderSetup> SpecialOrderSetup { get; set; }
        public DbSet<SysInputTable> SysInputTable { get; set; }
        public DbSet<SysInputDataField> SysInputDataField { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var configuration = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json")
               .Build();
            optionsBuilder.UseSqlServer(configuration["DbConnectString:DbConnectStringCentral"]);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<POSTerminalBank>(entity =>
            {
                entity.HasKey(e => new { e.TenderTypeCode, e.BankPOS });
            });
            modelBuilder.Entity<SumInvoiceInfo>(entity =>
            {
                entity.HasKey(e => new { e.Fkey, e.StoreCode, e.POSNo, e.ReceiptNo, e.EntryDate });
            });
            modelBuilder.Entity<SumInvoiceDetail>(entity =>
            {
                entity.HasKey(e => new { e.StoreCode,e.TemplateNumber,e.SerialNumber,e.DocumentNo, e.Type });
            });
            modelBuilder.Entity<Role>(entity =>
            {
                entity.HasKey(e => new { e.RoleID });
            });
            modelBuilder.Entity<LinkRolesMenus>(entity =>
            {
                entity.HasKey(e => new { e.Id });
            });
            modelBuilder.Entity<CustomerPriceGroup>(entity =>
            {
                entity.HasKey(e => new { e.Code });
            });
            modelBuilder.Entity<Function>(entity =>
            {
                entity.HasKey(e => new { e.Id });
            });
            modelBuilder.Entity<SpecialOrderSetup>(entity =>
            {
                entity.HasKey(e => new { e.Code });
            });

            modelBuilder.Entity<SysInputDataField>(entity =>
            {
                entity.HasKey(e => new { e.Code, e.LineNumber });
            });
            modelBuilder.Entity<CpnVchBOMHeader>(entity =>
            {
                entity.HasKey(e => new { e.CouponCode, e.ItemNo });
            });
            modelBuilder.Entity<CpnVchBOMLine>(entity =>
            {
                entity.HasKey(e => new { e.LineNo, e.ItemNo });
            });
            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.HasKey(e => new { e.UserID, e.RoleID });
            });
            modelBuilder.Entity<SysInputTable>(entity =>
            {
                entity.HasKey(e => new { e.Code });
            });

            modelBuilder.Entity<POSVersion>(entity =>
            {
                entity.HasKey(e => new { e.Key });
            });

            modelBuilder.Entity<POSSetupEndOfShift>(entity =>
            {
                entity.HasKey(e => new { e.Code });
            });

            modelBuilder.Entity<POSEndOfShiftLine>(entity =>
            {
                entity.HasKey(e => new { e.DocumentNo });
            });

            modelBuilder.Entity<POSEndOfShiftHeader>(entity =>
            {
                entity.HasKey(e => new { e.DocumentNo });
            });

            modelBuilder.Entity<POSEndOfDay>(entity =>
            {
                entity.HasKey(e => new { e.StoreNo, e.TransactionDate });
            });

            modelBuilder.Entity<POSDocumentNos>(entity =>
            {
                entity.HasKey(e => new { e.StoreNo, e.TransDate, e.POSTerminal });
            });

            modelBuilder.Entity<ItemUnitOfMeasure>(entity =>
            {
                entity.HasKey(e => new { e.ItemNo });
            });
            modelBuilder.Entity<TransVoidHeader>(entity =>
            {
                entity.HasKey(e => new { e.OrderNo });
            });
            modelBuilder.Entity<TransVoidLine>(entity =>
            {
                entity.HasKey(e => new { e.DocumentNo });
            });
            modelBuilder.Entity<UnitOfMeasure>(entity =>
            {
                entity.HasKey(e => new { e.Code });
            });
            modelBuilder.Entity<ItemMaxSalesQty>(entity =>
            {
                entity.HasKey(e => new { e.ItemNo, e.StoreGroup, e.UnitOfMeasure });
            });
            modelBuilder.Entity<LinkedItem>(entity =>
            {
                entity.HasKey(e => new { e.ItemNo, e.UnitOfMeasure, e.LinkedItemNo });
            });
            modelBuilder.Entity<SysMenu>(entity =>
            {
                entity.HasKey(e => new { e.Khoa });
            });
            modelBuilder.Entity<SysModules>(entity =>
            {
                entity.HasKey(e => new { e.Ma });
            });
            modelBuilder.Entity<TableSpecificInfocode>(entity =>
            {
                entity.HasKey(e => new { e.TableID, e.Value, e.InfocodeCode });
            });

            modelBuilder.Entity<Infocode>(entity =>
            {
                entity.HasKey(e => new { e.Code });
            });
            modelBuilder.Entity<Item>(entity =>
            {
                entity.HasKey(e => new { e.No });
            });
            modelBuilder.Entity<InformationSubcode>(entity =>
            {
                entity.HasKey(e => new { e.Code, e.Subcode });
            });
            modelBuilder.Entity<CpnVchBOMHeader>(entity =>
            {
                entity.HasKey(e => new { e.ItemNo, e.CouponCode });
            });
            modelBuilder.Entity<CpnVchBOMLine>(entity =>
            {
                entity.HasKey(e => new { e.ItemNo, e.LineNo });
            });
            modelBuilder.Entity<ItemEarnScale>(entity =>
            {
                entity.HasKey(e => new { e.ItemNo, e.StartingDate, e.StoreGroup });
            });
            modelBuilder.Entity<TenderTypeSetup>(entity =>
            {
                entity.HasKey(e => new { e.Code });
            });
            modelBuilder.Entity<TenderType>(entity =>
            {
                entity.HasKey(e => new { e.Code, e.StoreNo });
            });
            modelBuilder.Entity<Staff>(entity =>
            {
                entity.HasKey(e => new { e.ID });
            });
            modelBuilder.Entity<RetailSetup>(entity =>
            {
                entity.HasKey(e => new { e.Key });
            });
            modelBuilder.Entity<ReasonCode>(entity =>
            {
                entity.HasKey(e => new { e.Code });
            });
            modelBuilder.Entity<POSVATCode>(entity =>
            {
                entity.HasKey(e => new { e.VATCode });
            });
            modelBuilder.Entity<POSDataSetup>(entity =>
            {
                entity.HasKey(e => new { e.Code });
            });

            modelBuilder.Entity<Store>(entity =>
            {
                entity.HasKey(e => new { e.No });
            });

            modelBuilder.Entity<StorePriceGroup>(entity =>
            {
                entity.HasKey(e => new { e.Store, e.PriceGroupCode });
            });

            modelBuilder.Entity<POSHotKeyItem>(entity =>
            {
                entity.HasKey(e => new { e.StoreGroup, e.Barcode });
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => new { e.UserName });
            });

            modelBuilder.Entity<SalesPrice>(entity =>
            {
                entity.HasKey(e => new { e.ItemNo, e.UnitOfMeasureCode, e.SalesCode, e.StartingDate });
            });

            modelBuilder.Entity<POSTerminal>(entity =>
            {
                entity.HasKey(e => new { e.No });
            });
            modelBuilder.Entity<OfferBenefits>(entity =>
            {
                entity.HasKey(e => new { e.OfferNo });
            });
            modelBuilder.Entity<OfferBuy>(entity =>
            {
                entity.HasKey(e => new { e.OfferNo });
            });
            modelBuilder.Entity<OfferGet>(entity =>
            {
                entity.HasKey(e => new { e.OfferNo });
            });
            modelBuilder.Entity<OfferHeader>(entity =>
            {
                entity.HasKey(e => new { e.No });
            });

            modelBuilder.Entity<OfferSite>(entity =>
            {
                entity.HasKey(e => new { e.OfferNo, e.StoreNo });
            });

            modelBuilder.Entity<TransInfocodeEntry>(entity =>
            {
                entity.HasKey(e => new { e.OrderNo, e.LineNo, e.OrderLineNo });
            });

            modelBuilder.Entity<TransDiscountEntry>(entity =>
            {
                entity.HasKey(e => new { e.OrderNo, e.LineNo, e.OrderLineNo });
            });

            modelBuilder.Entity<TransPaymentEntry>(entity =>
            {
                entity.HasKey(e => new { e.OrderNo, e.LineNo });
            });

            modelBuilder.Entity<TransHeader>(entity =>
            {
                entity.HasKey(e => e.OrderNo);
            });

            modelBuilder.Entity<TransLine>(entity =>
            {
                entity.HasKey(e => new { e.DocumentNo, e.LineNo });
            });
            modelBuilder.Entity<ItemInfo>(entity =>
            {
                entity.HasKey(e => new { e.salesCode, e.itemNo });
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasKey(e => e.No);
            });

            modelBuilder.Entity<Currency>(entity =>
            {
                entity.HasKey(e => e.Code);
            });

            modelBuilder.Entity<CouponHeader>(entity =>
            {
                entity.HasKey(e => e.Code);
            });

            modelBuilder.Entity<CompanyInformation>(entity =>
            {
                entity.HasKey(e => e.PrimaryKey);
            });

            modelBuilder.Entity<Branch>(entity =>
            {
                entity.HasKey(e => e.No);
            });

            modelBuilder.Entity<Barcodes>(entity =>
            {
                entity.HasKey(e => new { e.BarcodeNo, e.ItemNo, e.UnitOfMeasureCode });
                entity.HasIndex(e => e.BarcodeNo);
            });

            modelBuilder.Entity<TransStatus>(entity =>
            {
                entity.HasKey(e => e.RowIndex);
            });
        }

    }
}  

    

