﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class SysInputTable
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public int IsInfoCode { get; set; }
        public string UrlValidation { get; set; }

    }
}
