﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class POSHotKeyItem
    {
        public string StoreGroup { get; set; }
        public string Barcode { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public int Seq { get; set; }

    }
}
