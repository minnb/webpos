﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class Branch
    {
        public string No { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string VATRegistrationNo { get; set; }
        public string PhoneNo { get; set; }
        public string FaxNo { get; set; }
        public string BankAccountNo { get; set; }
        public string BankName { get; set; }
        public string BankAddress { get; set; }
        public string BankAcountName { get; set; }
        public string VietnameseDescription { get; set; }
        public string VietnameseAddress { get; set; }
        public string UrlElecInvoice { get; set; }

    }
}
