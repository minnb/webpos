﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class SalesPrice
    {
        public string ItemNo { get; set; }
        public string SalesCode { get; set; }
        public DateTime StartingDate { get; set; }
        public string CurrencyCode { get; set; }
        public string UnitOfMeasureCode { get; set; }
        public decimal UnitPrice { get; set; }
        public Byte PriceIncludesVAT { get; set; }
        public Byte AllowInvoiceDisc { get; set; }
        public int SalesType { get; set; }
        public decimal MinimumQuantity { get; set; }
        public DateTime EndingDate { get; set; }
        public string VariantCode { get; set; }
        public Byte AllowLineDisc { get; set; }

    }
}
