﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class TaxInvoiceDataFile
    {
        public string TaxID { get; set; }
        public string TemplateNumber { get; set; }
        public string SerialNumber { get; set; }
        public string DocumentNo { get; set; }
        public int Type { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string Status { get; set; }
        public string ErrorMessage { get; set; }
        public string StoreCode { get; set; }
    }
}


