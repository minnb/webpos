﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class TenderType
    {
        public string StoreNo { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int Function { get; set; }
        public string ChangeTenderCode { get; set; }
        public int Rounding { get; set; }
        public decimal RoundingTo { get; set; }
        public decimal MinAmountEntered { get; set; }
        public decimal MaxAmountEntered { get; set; }
        public decimal MinAmountAllowed { get; set; }
        public decimal MaxAmountAllowed { get; set; }
        public Byte MayBeUsed { get; set; }
        public Byte ManagerKeyControl { get; set; }
        public Byte KeyboardEntryAllowed { get; set; }
        public Byte OvertenderAllowed { get; set; }
        public decimal OvertenderMaxAmt { get; set; }
        public Byte DrawerOpens { get; set; }
        public Byte CardAccountNo { get; set; }
        public Byte AskForDate { get; set; }
        public Byte ForeignCurrency { get; set; }
        public Byte FloatAllowed { get; set; }
        public int BankAccountType { get; set; }
        public string BankAccountNo { get; set; }
        public string BankAccountName { get; set; }
        public Byte ReturnVoucher { get; set; }
        public string AccountNo { get; set; }

    }
}
