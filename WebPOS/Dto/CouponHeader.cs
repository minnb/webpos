﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class CouponHeader
    {
        public string Code { get; set; }
        public string CouponIssuer { get; set; }
        public string CouponReferenceNo { get; set; }
        public string Description { get; set; }
        public string PriceGroup { get; set; }
        public int DiscountType { get; set; }
        public decimal DiscountValue { get; set; }
        public string ValidationPeriodID { get; set; }
        public int Status { get; set; }
        public int Type { get; set; }
        public int Handling { get; set; }
        public int NoOfItemsToTrigger { get; set; }
        public int ApplyToNoOfItems { get; set; }
        public int Affects { get; set; }
        public int LineType { get; set; }
        public string LineNo { get; set; }
        public string VariantCode { get; set; }
        public string LineDescription { get; set; }
        public string UnitOfMeasure { get; set; }
        public Byte IssueAtPOS { get; set; }
        public string LoyaltyScheme { get; set; }
        public decimal DiscountOfTotal { get; set; }
        public string RoundingMethod { get; set; }
        public string ExtraPrintSetup { get; set; }
        public string BarcodeMask { get; set; }
        public int MemberType { get; set; }
        public string MemberValue { get; set; }
        public Byte CreateOpenEntry { get; set; }
        public Byte IssueDateValidation { get; set; }
        public string FirstValidDateFormula { get; set; }
        public string LastValidDateFormula { get; set; }
        public int CalculationType { get; set; }
        public decimal Value { get; set; }
        public int EntryValidation { get; set; }
        public int PostingType { get; set; }
        public string PostingAccount { get; set; }
        public string BuyerID { get; set; }
        public string BuyerGroupCode { get; set; }
        public int OfflineProcess { get; set; }
        public int CouponIDMethod { get; set; }
        public int MaxPerMember { get; set; }
        public decimal MinimumTransAmount { get; set; }
        public decimal MaximumTransAmount { get; set; }

    }
}
