﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class POSDocumentNos
    {
        public string StoreNo { get; set; }
        public string POSTerminal { get; set; }
        public DateTime TransDate { get; set; }
        public string LastNumber { get; set; }
        public DateTime LastDateTime { get; set; }

    }
}
