﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebPOS.Dto
{
    public class POSEndOfDay
    {
        public string StoreNo { get; set; }
        public DateTime TransactionDate { get; set; }
        public DateTime PostingDate { get; set; }
        public decimal BeginingCashAmount { get; set; }
        public decimal CnCCashAmount { get; set; }
        public decimal HDCashAmount { get; set; }
        public decimal TotalCashAmount { get; set; }
        public string CreatedByUser { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public decimal HDCashOtherAmount { get; set; }
        public string PosNo { get; set; }
        public int SalesQty { get; set; }
        public Byte Async { get; set; }
        [Column("POSEndOfDay")]
        public Byte POSEndOf_Day { get; set; }
        public Byte StoreEndOfDay { get; set; }
        public int Status { get; set; }

    }
}
