﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class SysModules
    {
        public string Ma { get; set; }
        public string TenViet { get; set; }
        public string TenAnh { get; set; }
        public int ImgIndex { get; set; }
        public int Stt { get; set; }
        public int HeThong { get; set; }
        public int HienThi { get; set; }

    }
}
