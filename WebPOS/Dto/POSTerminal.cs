﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WebPOS.Dto
{
    public class POSTerminal
    {
        public string MACAddress { get; set; }
        
        public string No { get; set; }
        
        public string StoreNo { get; set; }
       
        public int TerminalType { get; set; }
        
        public string Description { get; set; }
        public string Placement { get; set; }
        public int StatementMethod { get; set; }
        public Byte TerminalStatement { get; set; }
        public string DefaultPriceGroup { get; set; }
        
        public string TerminalNetworkID { get; set; }
        public string TerminalIPAddress { get; set; }
        public int TerminalConnection { get; set; }
        public Byte ShowItemPictures { get; set; }
        public Byte ShowItemHtml { get; set; }
        public Byte DisplayTerminalClosed { get; set; }
        public Byte DisplayLinkedItem { get; set; }
        public int AutoLogoffAfter_Min { get; set; }
        public Byte ReturnInTransaction { get; set; }
        public int ItemNoOnReceipt { get; set; }
        public DateTime LastDateModified { get; set; }
        public string PrintReceiptLogo { get; set; }
        public int PrintReceiptBitmapNo { get; set; }
        public int RcptTextMaxLength { get; set; }
        public Byte ReceiptBarcode { get; set; }
        public int ReceiptSetupLocation { get; set; }
        public int DisplayTextMaxLength { get; set; }
        public string CustomerDisplayText1 { get; set; }
        public string CustomerDisplayText2 { get; set; }
        public int PrintReceiptBCType { get; set; }
        public int ReceiptBarcodeWidth { get; set; }
        public int ReceiptBarcodeHeight { get; set; }
        public string DefaultSalesType { get; set; }
        public int StaffLoginValidation { get; set; }
        public string HardwareProfile { get; set; }
        public string MenuProfile { get; set; }
        public string InterfaceProfile { get; set; }
        public string FunctionalityProfile { get; set; }
        public string StyleProfile { get; set; }
        public Byte PrintNumberOfItems { get; set; }
        public Byte PrintSecondReceipt { get; set; }
        public string SalesTypeFilter { get; set; }
        public string DualDisHost { get; set; }
        public string BillNoseri { get; set; }

    }
}
