﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WebPOS.Dto
{
    public class User
    {
        //internal object model;

        public string UserName { get; set; }
        public string FullName { get; set; }
        public int State { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string WindowsSecurityID { get; set; }
        public Byte ChangePassword { get; set; }
        public int LicenseType { get; set; }
        public string AuthenticationEmail { get; set; }

        public string StoreNo { get; set; }
    }
}
