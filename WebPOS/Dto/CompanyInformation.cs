﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class CompanyInformation
    {
        public string PrimaryKey { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PhoneNo { get; set; }
        public string PhoneNo2 { get; set; }
        public string TelexNo { get; set; }
        public string FaxNo { get; set; }
        public string GiroNo { get; set; }
        public string BankName { get; set; }
        public string BankBranchNo { get; set; }
        public string BankAccountNo { get; set; }
        public string PaymentRoutingNo { get; set; }
        public string CustomsPermitNo { get; set; }
        public DateTime CustomsPermitDate { get; set; }
        public string VATRegistrationNo { get; set; }
        public string RegistrationNo { get; set; }
        public string TelexAnswerBack { get; set; }
        public string ShipToName { get; set; }
        public string ShipToAddress { get; set; }
        public string ShipToCity { get; set; }
        public string ShipToContact { get; set; }
        public string LocationCode { get; set; }
        public string PostCode { get; set; }
        public string County { get; set; }
        public string ShipToPostCode { get; set; }
        public string ShipToCounty { get; set; }
        public string EMail { get; set; }
        public string HomePage { get; set; }
        public string CountryRegionCode { get; set; }
        public string ShipToCountryRegionCode { get; set; }
        public string IBAN { get; set; }
        public string SWIFTCode { get; set; }
        public string IndustrialClassification { get; set; }
        public string ICPartnerCode { get; set; }
        public int ICInboxType { get; set; }
        public string ICInboxDetails { get; set; }
        public int SystemIndicator { get; set; }
        public string CustomSystemIndicatorText { get; set; }
        public int SystemIndicatorStyle { get; set; }
        public Byte AllowBlankPaymentInfo { get; set; }
        public string ResponsibilityCenter { get; set; }
        public Byte CheckAvailPeriodCalc { get; set; }
        public int CheckAvailTimeBucket { get; set; }
        public string BaseCalendarCode { get; set; }
        public Byte CalConvergenceTimeFrame { get; set; }

    }
}
