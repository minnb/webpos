﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class RetailSetup
    {
        public string Key { get; set; }
        public string LocalStoreNo { get; set; }
        public Byte LSRetailInUse { get; set; }
        public string EANLicenseNo { get; set; }
        public string DefVATBusPostGrPrice { get; set; }
        public Byte DefPriceIncludesVAT { get; set; }
        public string SourceCode { get; set; }
        public string StoreNoNos { get; set; }
        public int ItemSalesStatisticsOn { get; set; }
        public Byte POSTerminalStatistics { get; set; }
        public Byte StaffStatistics { get; set; }
        public Byte PaymentStatistics { get; set; }
        public DateTime LastDateModified { get; set; }
        public int DaysBeforeTransArchive { get; set; }
        public Byte CommissionActive { get; set; }
        public Byte CalculateInStatemPosting { get; set; }
        public Byte CalculateInSalesPosting { get; set; }
        public Byte ExcludeReturns { get; set; }
        public int BalAccType { get; set; }
        public string BalAccNo { get; set; }
        public Byte OriginalSalespersinReturns { get; set; }
        public Byte Dimension1Mandatory { get; set; }
        public Byte OnlyTwoDimensions { get; set; }
        public int xNumberofRetries { get; set; }
        public string DefaultPriceGroup { get; set; }
        public Byte PostTotalDisc { get; set; }
        public Byte PostInfocodeDisc { get; set; }
        public Byte PostLineDisc { get; set; }
        public Byte PostPeriodicDisc { get; set; }
        public Byte PostCustDisc { get; set; }
        public Byte PostCouponDisc { get; set; }
        public Byte PostLineDiscOffer { get; set; }
        public Byte PostTotalDiscOffer { get; set; }
        public Byte PostTenderTypeDisc { get; set; }
        public int ItemPostingDate { get; set; }
        public int DefaultCustomerPosting { get; set; }
        public Byte UpdateCostAmount { get; set; }
        public Byte ItemLabelsForNegStock { get; set; }
        public Byte ItemLabelsOnPriceChange { get; set; }
        public Byte PostAlwaysReserveItems { get; set; }
        public Byte DeletePrintedLabels { get; set; }
        public int POItemLookupMethod { get; set; }
        public decimal Difference { get; set; }
        public int AutocreateBarcodes { get; set; }
        public string CreateItemsNoSeries { get; set; }
        public string DefaultItemHierarchy { get; set; }
        public string DistributionLocation { get; set; }
        public string DefStoreHierarchy { get; set; }
        public Byte AllowRename { get; set; }

    }
}
