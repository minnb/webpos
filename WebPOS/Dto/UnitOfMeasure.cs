﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class UnitOfMeasure
    {
        public string Code { get; set; }
        public string Description { get; set; }

    }
}
