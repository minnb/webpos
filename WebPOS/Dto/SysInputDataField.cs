﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class SysInputDataField
    {
        public string Code { get; set; }
        public int LineNumber { get; set; }
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public string FieldDescription { get; set; }
        public string DataType { get; set; }
        public int Seq { get; set; }
        public string TableLink { get; set; }
        public string FieldLink { get; set; }
        public Byte IsKey { get; set; }

    }
}
