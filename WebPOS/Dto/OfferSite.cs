﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class OfferSite
    {
        public string OfferNo { get; set; }
        public string PriceGroupCode { get; set; }
        public string StoreNo { get; set; }

    }
}
