﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class OfferHeader
    {
        public string No { get; set; }
        public int Type { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public string OfferType { get; set; }
        public string PriceGroup { get; set; }
        public string RoundingMethod { get; set; }
        public string CurrencyCode { get; set; }
        public DateTime LastDateModified { get; set; }
        public string ValidationPeriodID { get; set; }
        public string ValidationDescription { get; set; }
        public DateTime StartingDate { get; set; }
        public DateTime EndingDate { get; set; }
        public Byte BlockPeriodicDiscount { get; set; }
        public decimal DealPrice { get; set; }
        public int ShowDealLines { get; set; }
        public string SalesTypeFilter { get; set; }
        public int SelectionType { get; set; }
        public string CustomerDiscGroup { get; set; }
        public string MemberValue { get; set; }
        public string DiscountTrackingNo { get; set; }
        public string CouponCode { get; set; }
        public decimal CouponQtyNeeded { get; set; }
        public int MemberType { get; set; }
        public string MemberAttribute { get; set; }
        public string MemberAttributeValue { get; set; }
        public Byte BlockSalesCommission { get; set; }
        public Byte BlockManualPriceChange { get; set; }
        public Byte BlockInfoCodeDiscount { get; set; }
        public Byte BlockLineDiscountOffer { get; set; }
        public Byte BlockTotalDiscountOffer { get; set; }
        public Byte BlockTenderTypeDiscount { get; set; }
        public Byte BlockMemberPoints { get; set; }
        public int ConditionBuy { get; set; }
        public Byte MemberOnly { get; set; }
        public int ConditionGet { get; set; }
        public string NoSeries { get; set; }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public Byte Mon { get; set; }
        public Byte Tue { get; set; }
        public Byte Wed { get; set; }
        public Byte Thu { get; set; }
        public Byte Fri { get; set; }
        public Byte Sat { get; set; }
        public Byte Sun { get; set; }
        public int NumOfDays { get; set; }
        public int Counter { get; set; }
        public string DayOfWeek { get; set; }
        public string TenderTypeCode { get; set; }
        public decimal TenderTypeValue { get; set; }
        public decimal TenderTypeOfferPercent { get; set; }
        public decimal TenderTypeOfferAmount { get; set; }
        public string BankCode { get; set; }
        public string LocalSiteGroup { get; set; }
        public decimal LimitQty { get; set; }

    }
}
