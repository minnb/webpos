﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class ItemEarnScale
    {
        public string StoreGroup { get; set; }
        public string StartingDate { get; set; }
        public string ItemNo { get; set; }
        public string EndingDate { get; set; }
        public decimal Scale { get; set; }
    }
}
