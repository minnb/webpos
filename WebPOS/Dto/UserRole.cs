﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPOS.Dto
{
    public class UserRole
    {
        public string UserID { get; set; }
        public string RoleID { get; set; }
        public string Status { get; set; }
    }
}
