﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPOS.Dto
{
    public class POSTerminalBank
    {
        public string TenderTypeCode { get; set; }

        public string BankPOS { get; set; }

        public string Description { get; set; }

        public string BankCode { get; set; }

        public string StoreNo { get; set; }
        public string POSTerminal { get; set; }
        public string StoreNumber { get; set; }
        public string POSNumber { get; set; }

    }
}
