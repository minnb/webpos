﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class POSSetupEndOfShift
    {
        public string Code { get; set; }
        public decimal Value { get; set; }
        public string Description { get; set; }
        public int SeqDisplay { get; set; }

    }
}
