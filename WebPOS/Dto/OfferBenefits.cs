﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class OfferBenefits
    {
        public string OfferNo { get; set; }
        public int LineNo { get; set; }
        public int Type { get; set; }
        public string No { get; set; }
        public string VariantCode { get; set; }
        public string Description { get; set; }
        public int ValueType { get; set; }
        public decimal Value { get; set; }
        public decimal StepAmount { get; set; }
        public string LineGroup { get; set; }
        public int Quantity { get; set; }
        public string UnitOfMeasure { get; set; }

    }
}
