﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class TransVoidLine
    {
        public int LineNo { get; set; }
        public string DocumentNo { get; set; }
        public int LineType { get; set; }
        public string LocationCode { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string UnitOfMeasure { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal OfferUnitPrice { get; set; }
        public decimal AmountBeforeDiscount { get; set; }
        public decimal DiscountPercent { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal VATPercent { get; set; }
        public decimal VATAmount { get; set; }
        public decimal LineAmountIncVAT { get; set; }
        public string OfferNo { get; set; }
        public int DiscountType { get; set; }
        public decimal TriggerQuantity { get; set; }
        public string StaffID { get; set; }
        public decimal PrepaymentAmount { get; set; }
        public string VATCode { get; set; }
        public int DeliveringMethod { get; set; }
        public string Barcode { get; set; }
        public decimal OrderDiscountPercent { get; set; }
        public decimal OrderDiscountAmount { get; set; }
        public string DivisionCode { get; set; }
        public string CategoryCode { get; set; }
        public string ProductGroupCode { get; set; }
        public Byte BlockedMemberPoint { get; set; }
        public string SerialNo { get; set; }
        public string OrigTransStore { get; set; }
        public string OrigTransPos { get; set; }
        public int OrigTransNo { get; set; }
        public int OrigTransLineNo { get; set; }
        public string OrigOrderNo { get; set; }
        public int OrigLineNumber { get; set; }
        public Byte BlockedPromotion { get; set; }
        public string Infocodes { get; set; }
        public decimal MemberPointsEarn { get; set; }
        public decimal ReturnedQuantity { get; set; }
        public decimal DeliveryQuantity { get; set; }
        public int DeliveryStatus { get; set; }
        public decimal MemberPointsRedeem { get; set; }
        public string VariantNo { get; set; }
        public string LotNo { get; set; }
        public Byte ExpireDate { get; set; }
        public decimal AmountCalPoint { get; set; }
        public string WaitingListNo { get; set; }
        public string ArticleType { get; set; }
        public Byte ScanTime { get; set; }

    }
}
