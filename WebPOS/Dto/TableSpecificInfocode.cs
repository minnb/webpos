﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebPOS.Dto
{
    public class TableSpecificInfocode
    {
        public int TableID { get; set; }
        public string Value { get; set; }
        public string InfocodeCode { get; set; }
        public int Sequence { get; set; }
        public string PrimaryKey { get; set; }
        public string SalesTypeFilter { get; set; }
        public string UnitOfMeasure { get; set; }
        [Column("Quantity Handling")]
        public int QuantityHandling {get;set; }
        [Column("When Required")]
        public int WhenRequired {get;set; }
        public int Triggering { get; set; }
        public int TableName { get; set; }
        public int UsageCategory { get; set; }
        public int UsageSubCategory { get; set; }

    }
}
