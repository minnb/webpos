﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class CpnVchBOMLine
    {
        public string ItemNo { get; set; }
        public int LineNo { get; set; }
        public string LineItemNo { get; set; }
        public string Description { get; set; }
        public string UnitOfMeasure { get; set; }
    }
}
