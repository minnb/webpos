﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPOS.Dto
{
    public class Function
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public int ParentId { get; set; }

        public int? SortOrder { get; set; }

        public string CssClass { get; set; }

        public bool IsActive { get; set; }
        
    }
}
