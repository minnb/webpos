﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class POSVersion
    {
        public string Key { get; set; }
        public string LastVersion { get; set; }
        public string NewVersion { get; set; }
        public int MustReplace { get; set; }

    }
}
