﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class SysMenu
    {
        public string Khoa { get; set; }
        public string TenViet { get; set; }
        public string TenAnh { get; set; }
        public int ImageIndex { get; set; }
        public string Nhom { get; set; }
        public string ThuocTinh { get; set; }
        public string MenuOrder { get; set; }
        public string PhanHe { get; set; }
        public string HeThong { get; set; }
        public int HienThi { get; set; }
        public int NhomMenu { get; set; }
        public string ObjectName { get; set; }

    }
}
