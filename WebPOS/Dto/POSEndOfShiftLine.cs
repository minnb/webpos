﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class POSEndOfShiftLine
    {
        public string DocumentNo { get; set; }
        public int LineNo { get; set; }
        public string CashCode { get; set; }
        public decimal CashValue { get; set; }
        public int Quantity { get; set; }
        public decimal CashAmount { get; set; }
        public string CashDescription { get; set; }

    }
}
