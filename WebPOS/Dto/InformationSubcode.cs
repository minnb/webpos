﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class InformationSubcode
    {
        public string Code { get; set; }
        public string Subcode { get; set; }
        public string Description { get; set; }

    }
}
