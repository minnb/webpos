﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class Currency
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public DateTime LastDateModified { get; set; }
        public DateTime LastDateAdjusted { get; set; }
        public decimal InvoiceRoundingPrecision { get; set; }
        public int InvoiceRoundingType { get; set; }
        public decimal AmountRoundingPrecision { get; set; }
        public string AmountDecimalPlaces { get; set; }
        public Byte EMUCurrency { get; set; }
        public decimal CurrencyFactor { get; set; }
        public string ResidualGainsAccount { get; set; }
        public string ResidualLossesAccount { get; set; }
        public int VATRoundingType { get; set; }
        public string CurrencyUnit { get; set; }
        public string CurrencySubUnit { get; set; }
        public string POSCurrencySymbol { get; set; }
        public int PlacementOfCurrencySymbol { get; set; }
        public int FiscalPrinterID { get; set; }

    }
}
