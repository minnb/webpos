﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class Customer
    {
        public string No { get; set; }
        public string Name { get; set; }
        public string SearchName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Contact { get; set; }
        public string PhoneNo { get; set; }
        public string TelexNo { get; set; }
        public string OurAccountNo { get; set; }
        public string TerritoryCode { get; set; }
        public string GlobalDimension1Code { get; set; }
        public string GlobalDimension2Code { get; set; }
        public string ChainName { get; set; }
        public decimal BudgetedAmount { get; set; }
        public decimal CreditLimit { get; set; }
        public string CustomerPostingGroup { get; set; }
        public string CurrencyCode { get; set; }
        public string CustomerPriceGroup { get; set; }
        public string LanguageCode { get; set; }
        public int StatisticsGroup { get; set; }
        public string PaymentTermsCode { get; set; }
        public string FinChargeTermsCode { get; set; }
        public string SalespersonCode { get; set; }
        public string ShipmentMethodCode { get; set; }
        public string ShippingAgentCode { get; set; }
        public string PlaceOfExport { get; set; }
        public string InvoiceDiscCode { get; set; }
        public string CustomerDiscGroup { get; set; }
        public string CountryRegionCode { get; set; }
        public string CollectionMethod { get; set; }
        public decimal Amount { get; set; }
        public int Blocked { get; set; }
        public int InvoiceCopies { get; set; }
        public int LastStatementNo { get; set; }
        public Byte PrintStatements { get; set; }
        public string BillToCustomerNo { get; set; }
        public int Priority { get; set; }
        public string PaymentMethodCode { get; set; }
        public DateTime LastDateModified { get; set; }
        public int ApplicationMethod { get; set; }
        public Byte PricesIncludingVAT { get; set; }
        public string LocationCode { get; set; }
        public string FaxNo { get; set; }
        public string TelexAnswerBack { get; set; }
        public string VATRegistrationNo { get; set; }
        public Byte CombineShipments { get; set; }
        public string GenBusPostingGroup { get; set; }
        public string PostCode { get; set; }
        public string County { get; set; }
        public string EMail { get; set; }
        public string HomePage { get; set; }
        public string ReminderTermsCode { get; set; }
        public string NoSeries { get; set; }
        public string TaxAreaCode { get; set; }
        public Byte TaxLiable { get; set; }
        public string VATBusPostingGroup { get; set; }
        public int Reserve { get; set; }
        public Byte BlockPaymentTolerance { get; set; }
        public string ICPartnerCode { get; set; }
        public decimal Prepayment { get; set; }
        public int PartnerType { get; set; }
        public string PreferredBankAccount { get; set; }
        public string CashFlowPaymentTermsCode { get; set; }
        public string PrimaryContactNo { get; set; }
        public string ResponsibilityCenter { get; set; }
        public int ShippingAdvice { get; set; }
        public string ShippingTime { get; set; }
        public string ShippingAgentServiceCode { get; set; }
        public string ServiceZoneCode { get; set; }
        public Byte AllowLineDisc { get; set; }
        public string BaseCalendarCode { get; set; }
        public int CopySellToAddrToQteFrom { get; set; }
        public int ReplicationCounter { get; set; }
        public string LogisticZone { get; set; }
        public string DistrictCode { get; set; }
        public string WardCode { get; set; }
        public string Street { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyTaxCode { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedByUser { get; set; }
        public string CustomerID { get; set; }
        public string ReasonCode { get; set; }
        public int RestrictionFunctionality { get; set; }
        public Byte PrintDocumentInvoice { get; set; }
        public decimal TransactionLimit { get; set; }
        public string DaytimePhoneNo { get; set; }
        public string MobilePhoneNo { get; set; }
        public string HouseNo { get; set; }
        public string RetailCustomerGroup { get; set; }
        public decimal DefaultWeight { get; set; }
        public Byte OtherTenderInFinalizing { get; set; }
        public Byte PostAsShipment { get; set; }

    }
}
