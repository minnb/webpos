﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class TransRaw
    {
        public string CompanyCode { get; set; }
        public string StoreCode { get; set; }
        public DateTime EntryDate { get; set; }
        public string TranNo { get; set; }
        public string PosNo { get; set; }
        public DateTime DateInsert { get; set; }
        public string RawData { get; set; }
        public string IPAddress { get; set; }
        public string UpdateFlg { get; set; }
        public string MACAddress { get; set; }
    }
}
