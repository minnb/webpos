﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class TransInfocodeEntry
    {
        public string OrderNo { get; set; }
        public int OrderLineNo { get; set; }
        public int LineNo { get; set; }
        public int TransactionType { get; set; }
        public string Infocode { get; set; }
        public string Infomation { get; set; }
        public int TypeOfInput { get; set; }
        public int TextType { get; set; }
        public string ItemNo { get; set; }
        public string SourceCode { get; set; }
        public decimal Amount { get; set; }
        public string SubCode { get; set; }
        public int ParentLineNo { get; set; }

    }
}
