﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class Infocode
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public int DisplayOption { get; set; }
        public int OncePerTransaction { get; set; }
        public int ValueIsAmtQty { get; set; }
        public int Type { get; set; }
        public int UsageCategory { get; set; }
        public int UsageSubCategory { get; set; }
        public decimal MinValue { get; set; }
        public decimal MaxValue { get; set; }
        public int MinLength { get; set; }
        public int MaxLength { get; set; }
        public int InputRequired { get; set; }
        public string LinkedInfocode { get; set; }
        public string DataEntryType { get; set; }
        public string InputTableCode { get; set; }
    }
}
