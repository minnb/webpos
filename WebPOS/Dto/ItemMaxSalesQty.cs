﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class ItemMaxSalesQty
    {
        public string ItemNo { get; set; }
        public string UnitOfMeasure { get; set; }
        public string StoreGroup { get; set; }
        public decimal MaxQuantity { get; set; }

    }
}
