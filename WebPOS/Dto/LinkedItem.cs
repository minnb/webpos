﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class LinkedItem
    {
        public string ItemNo { get; set; }
        public string UnitOfMeasure { get; set; }
        public string LinkedItemNo { get; set; }
        public decimal NoOfItems { get; set; }
        public Byte Blocked { get; set; }
        public string PrimaryKey { get; set; }
        public string SalesType { get; set; }
        public Byte DepositItem { get; set; }

    }
}
