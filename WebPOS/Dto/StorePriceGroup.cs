﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class StorePriceGroup
    {
        public string Store { get; set; }
        public string PriceGroupCode { get; set; }
        public int Type { get; set; }
        public int Priority { get; set; }
        public int ReplicationCounter { get; set; }
    }
}
