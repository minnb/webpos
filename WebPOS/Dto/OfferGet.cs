﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class OfferGet
    {
        public string OfferNo { get; set; }
        public int LineNo { get; set; }
        public int LineType { get; set; }
        public string No { get; set; }
        public string Description { get; set; }
        public string UnitOfMeasure { get; set; }
        public int DiscountType { get; set; }
        public decimal DiscountValue { get; set; }
        public decimal Quantity { get; set; }
        public decimal Step { get; set; }
        public string BonusBuyNo { get; set; }
        public string LineGroup { get; set; }
        public string ScaleType { get; set; }

    }
}
