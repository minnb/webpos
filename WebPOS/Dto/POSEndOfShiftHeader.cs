﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class POSEndOfShiftHeader
    {
        public string DocumentNo { get; set; }
        public string StoreNo { get; set; }
        public string CashierID { get; set; }
        public string POSTerminal { get; set; }
        public DateTime ShiftDate { get; set; }
        public int ShiftNo { get; set; }
        public decimal TotalAmount { get; set; }
        public string CreatedByUser { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string ModifiedByUser { get; set; }
        public DateTime ModifiedDateTime { get; set; }
        public int ShiftStatus { get; set; }

    }
}
