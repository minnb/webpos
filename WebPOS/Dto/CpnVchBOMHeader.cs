﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class CpnVchBOMHeader
    {
      public string ItemNo { get; set; }
      public string ItemName { get; set; }
        public string UnitOfMeasure { get; set; }
        public int DiscountType { get; set; }
        public decimal DiscountValue { get; set; }
        public decimal MaxAmount { get; set; }
        public string ArticleType { get; set; }
        public decimal ValueOfVoucher { get; set; }
        public DateTime StartingDate { get; set; }
        public DateTime EndingDate { get; set; }
        public byte Blocked { get; set; }
        public string CouponCode { get; set; }
        public int LimitQty { get; set; }
        public DateTime LastDateModified { get; set; }
    }
}
