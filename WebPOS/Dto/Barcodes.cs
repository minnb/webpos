﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class Barcodes
    {
        public string BarcodeNo { get; set; }
        public string ItemNo { get; set; }
        public Byte ShowForItem { get; set; }
        public string Description { get; set; }
        public Byte Blocked { get; set; }
        public DateTime LastDateModified { get; set; }
        public string VariantCode { get; set; }
        public string UnitOfMeasureCode { get; set; }
        public decimal DiscountPercent { get; set; }

    }
}
