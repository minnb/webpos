﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
   public class SumInvoiceInfo
    {
        public string Fkey { get; set; }
        public string StoreCode { get; set; }
        public string POSNo { get; set; }
        public string ReceiptNo { get; set; }
        public string EntryDate { get; set; }
        public string StaffNo { get; set; }
        public string Returned { get; set; }
        public string SeriesFlg { get; set; }
        public string DocumentNo { get; set; }
        public int  Type { get; set; }
        public DateTime? PrintedDate { get; set; }
        public DateTime? EditedDate { get; set; }
        public string CreatorCode { get; set; }
        public string OriginalDocumentNo { get; set; }
        public string Remark { get; set; }
        public string TemplateNumber { get; set; }
        public string SerialNumber { get; set; }
        public string OriginalTemplateNumber { get; set; }
        public string OriginalSerialNumber { get; set; }
        public string TranSendFlg { get; set; }
        public string UpdateFlg { get; set; }
        public string Pos_No { get; set; }
        public string Receipt_No { get; set; }
        public int OriginalType { get; set; }
        public string AdjustedTemplateNumber { get; set; }
        public string AdjustedSerialNumber { get; set; }
        public string AdjustedDocumentNo { get; set; }
    }
}
