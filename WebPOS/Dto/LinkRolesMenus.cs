﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPOS.Dto
{
    public class LinkRolesMenus
    {
        public int Id { get; set; }
        public string RoleID { get; set; }
        public int FunctionID { get; set; }
       
    }
}
