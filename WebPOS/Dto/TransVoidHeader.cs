﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class TransVoidHeader
    {
        public string OrderNo { get; set; }
        public DateTime OrderDate { get; set; }
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string PhoneNo { get; set; }
        public string TablePhoneNo { get; set; }
        public string HouseNo { get; set; }
        public string CityNo { get; set; }
        public string DistrictNo { get; set; }
        public string WardNo { get; set; }
        public string StreetNo { get; set; }
        public string ShipToCityNo { get; set; }
        public string ShipToDistrictNo { get; set; }
        public string ShipToWardNo { get; set; }
        public string ShipToStreetNo { get; set; }
        public DateTime DeliveryDate { get; set; }
        public DateTime DeliveryTimeFrom { get; set; }
        public DateTime DeliveryTimeTo { get; set; }
        public string ZoneNo { get; set; }
        public string ShipToAddress { get; set; }
        public string DeliveryComment { get; set; }
        public string BillToName { get; set; }
        public string BillToAddress { get; set; }
        public string VATRegistrationNo { get; set; }
        public string StoreNo { get; set; }
        public string POSTerminalNo { get; set; }
        public string ShiftNo { get; set; }
        public string CashierID { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal AmountExclVAT { get; set; }
        public decimal VATAmount { get; set; }
        public decimal AmountInclVAT { get; set; }
        public string GeneralComment { get; set; }
        public string UserID { get; set; }
        public decimal PrepaymentAmount { get; set; }
        public int DeliveringMethod { get; set; }
        public decimal PaymentAtPOSAmount { get; set; }
        public decimal InChangeAmount { get; set; }
        public int OrderStatus { get; set; }
        public string ShipToName { get; set; }
        public Byte IsTenancy { get; set; }
        public Byte InInstalments { get; set; }
        public string ShipToHouseNo { get; set; }
        public string ShipToPhoneNo { get; set; }
        public decimal AmountDiscountAtPOS { get; set; }
        public Byte IssuedVATInvoice { get; set; }
        public string VoucherDiscountNo { get; set; }
        public decimal VoucherDiscountValue { get; set; }
        public decimal PointConversionRate { get; set; }
        public string TanencyNo { get; set; }
        public int StepProcess { get; set; }
        public Byte SalesIsReturn { get; set; }
        public string MemberCardNo { get; set; }
        public decimal MemberPoint { get; set; }
        public string ReturnedReceiptNo { get; set; }
        public string ReturnedOrderNo { get; set; }
        public string VATNumber { get; set; }
        public string VATTemplateCode { get; set; }
        public string VATSerial { get; set; }
        public int TransactionType { get; set; }
        public string EventNo { get; set; }
        public int PrintedNumber { get; set; }
        public DateTime OrderTime { get; set; }
        public Byte IsFullReturn { get; set; }
        public string SendingStatus { get; set; }
        public string BuyerName { get; set; }
        public string BillNumber { get; set; }
        public DateTime StartingTime { get; set; }
        public DateTime EndingTime { get; set; }
        public string RefKey1 { get; set; }
        public string RefKey2 { get; set; }

    }
}
