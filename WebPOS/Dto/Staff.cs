﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class Staff
    {
        public string ID { get; set; }
        public string Password { get; set; }
        public Byte ChangePassword { get; set; }
        public string StoreNo { get; set; }
        public int VoidTransaction { get; set; }
        public int ManagerPrivileges { get; set; }
        public int TenderDeclaration { get; set; }
        public int FloatingDeclaration { get; set; }
        public int PriceOverride { get; set; }
        public decimal MaxDiscountToGive { get; set; }
        public int SuspendTransaction { get; set; }
        public decimal MaxTotalDiscount { get; set; }
        public int OpenDrawWithoutSale { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int EmploymentType { get; set; }
        public decimal FraudSortField { get; set; }
        public DateTime LastDateModified { get; set; }
        public string NameOnReceipt { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string HomePhoneNo { get; set; }
        public string WorkPhoneNo { get; set; }
        public decimal HourlyRate { get; set; }
        public string PayrollNo { get; set; }
        public Byte Blocked { get; set; }
        public DateTime DateToBeBlocked { get; set; }
        public Byte LeftHanded { get; set; }
        public string SalesPerson { get; set; }
        public string PermissionGroup { get; set; }
        public int ReturnInTransaction { get; set; }
        public int VoidPrepayment { get; set; }
        public int VoidPrepaymentLine { get; set; }
        public int ChangePrepaymentAmt { get; set; }
        public int AddPrepaymentAmt { get; set; }
        public int VoidLine { get; set; }
        public int AddPayment { get; set; }
        public int SplitBills { get; set; }
        public string Language { get; set; }
        public int CreateCustomers { get; set; }
        public int ViewSalesHistory { get; set; }
        public int UpdateCustomers { get; set; }
        public Byte InventoryActive { get; set; }
        public string InventoryMainMenu { get; set; }
        public string POSStyleProfile { get; set; }
        public string POSMenuProfile { get; set; }
        public int DeliverStatus { get; set; }

    }
}
