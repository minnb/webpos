﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebPOS.Dto
{
    public class CustomerPriceGroup
    {
        public string Code { get; set; }
        public Byte PriceIncludesVAT { get; set; }
        public Byte AllowInvoiceDisc { get; set; }
        public string VATBusPostingGr { get; set; }
        public string Description { get; set; }
        [Column("AllowLine Disc_")]
        public Byte AllowLineDisc_ {get;set; }
        public string StoreGroup { get; set; }
        public int DefaultPriority { get; set; }
        public Byte ForecourtPriceGroup { get; set; }
        public decimal MaxPriceChange { get; set; }
        public Byte RetailPriceGroup { get; set; }
    }
}
