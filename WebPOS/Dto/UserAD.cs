﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPOS.Dto
{
    public class UserAD
    {
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public string DisplayName { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string MiddleName { get; set; }
        public string SamAccountName { get; set; }
        public string UserPrincipalName { get; set; }
        public string VoiceTelephoneNumber { get; set; }
        public string Sid { get; set; }
        public string Guid { get; set; }
    }
}
