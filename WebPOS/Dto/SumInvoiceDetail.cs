﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class SumInvoiceDetail
    {
        public string ProvinceTaxID { get; set; }
        public string StoreCode { get; set; }
        public string TemplateNumber { get; set; }
        public string SerialNumber { get; set; }
        public string DocumentNo { get; set; }
        public int Type { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string Fkey { get; set; }
        public string Status { get; set; }
        public string EntryDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string OriginalTemplateNumber { get; set; }
        public string OriginalSerialNumber { get; set; }
        public string OriginalDocumentNo { get; set; }
        public string CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string TaxID { get; set; }
        public string Branch { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string EmailDeliver { get; set; }
        public string Voided { get; set; }
        public string PrimaryTemplateNumber { get; set; }
        public string PrimarySerialNumber { get; set; }
        public string PrimaryDocumentNo { get; set; }
        public string Returned { get; set; }
        public int OriginalType { get; set; }
        public string AdjustedTemplateNumber { get; set; }
        public string AdjustedSerialNumber { get; set; }
        public string AdjustedDocumentNo { get; set; }
        public int AdjustedType { get; set; }
        public string QueryCode { get; set; }
    }
}
