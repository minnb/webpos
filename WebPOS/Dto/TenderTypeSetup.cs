﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class TenderTypeSetup
    {

        public string Code { get; set; }
        public string Description { get; set; }
        public int DefaultFunction { get; set; }
        public Byte DefaultCardTender { get; set; }
        public Byte DefaultCurrencyTender { get; set; }
        public string Caption { get; set; }
        public int SeqOnPOS { get; set; }
        public Byte IsInstallmentSell { get; set; }
        public int PaymentMethod { get; set; }
        public string RefKey1 { get; set; }
        public string RefKey2 { get; set; }

        public string CurrencyCode { get; set; }
    }
}
