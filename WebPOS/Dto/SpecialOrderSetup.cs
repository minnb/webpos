﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class SpecialOrderSetup
    {
        public string Code { get; set; }
        public string VendorItemLibraryNos { get; set; }
        public string LibraryDefLotNosSeries { get; set; }
        public string OptionConfigurationNos { get; set; }
        public string LibraryWorkshNoSeries { get; set; }
        public string FixedOptionUOM { get; set; }
        public Byte FixedOptionPriceIncVAT { get; set; }
        public string DefaultInTransitCode { get; set; }
        public Byte ShowOnlyAvailToUseOnPOS { get; set; }
        public string BuyerID { get; set; }
        public string BuyerGroupCode { get; set; }
        public string SpecialOrderSourceCode { get; set; }
        public Byte SameAddressInShipping { get; set; }
        public Byte ValidateCustomerBalance { get; set; }
        public Byte CreateItemOnPOS { get; set; }
        public Byte ShowDetailsOnTotal { get; set; }
        public string CustomerNos { get; set; }
        public string DefaultCustPostingGrp { get; set; }
        public int DefaultItemCostingMethod { get; set; }
        public Byte UseItemLibraryUnitPrice { get; set; }
        public Byte UseItemLibraryUnitCost { get; set; }
        public string UseOneCustomerForContacts { get; set; }
        public string ContactNos { get; set; }
        public string NonRefundG_LAccountNo { get; set; }
        public string RefundG_LAccountNo { get; set; }
        public Byte RunProcessTransaction { get; set; }
        public Byte RunSourcingCheck { get; set; }
        public Byte RunSourcing { get; set; }
        public Byte RunTransfer { get; set; }
        public Byte RunProcessOrder { get; set; }
        public Byte PhoneNoRequired { get; set; }

    }
}
