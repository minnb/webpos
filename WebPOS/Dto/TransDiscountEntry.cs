﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class TransDiscountEntry
    {
        public string OrderNo { get; set; }
        public int OrderLineNo { get; set; }
        public int LineNo { get; set; }
        public string OfferType { get; set; }
        public string OfferNo { get; set; }
        public float Quantity { get; set; }
        public int DiscountType { get; set; }
        public decimal DiscountAmount { get; set; }
        public string Barcode { get; set; }
        public int ParentLineNo { get; set; }
        public string ItemNo { get; set; }

    }
}
