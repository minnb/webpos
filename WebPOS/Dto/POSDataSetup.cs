﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class POSDataSetup
    {
        public string Code { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string StoreNo { get; set; }

    }
}
