﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class ReasonCode
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Group { get; set; }
        public int HandpointCode { get; set; }

    }
}
