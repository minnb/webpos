﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebPOS.Dto
{
    [Table("TransStatus")]
    public class TransStatus
    {
        public string OrderNo { get; set; }
        public int Status { get; set; }
        public string Crt_User { get; set; }
        public string ChgeUser  { get; set; }
        public DateTime Crt_Date  { get; set; }
        public DateTime ChgeDate  { get; set; }
        public string RowIndex { get; set; }
    }
}
