﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPOS.Dto
{
    public class POSVATCode
    {
        public string VATCode { get; set; }
        public string Description { get; set; }
        public decimal VATPercent { get; set; }
        public int FiscalID { get; set; }

    }
}
