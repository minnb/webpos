﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPOS.Dto
{
    public class ItemInfo
    {
        public string salesCode { get; set; }
        public string itemNo { get; set; }
        public string barcodeNo { get; set; }
        public string description { get; set; }
        public string unitOfMeasureCode { get; set; }
        public int unitPrice { get; set; }
        public string articleType { get; set; }
        public DateTime startingDate { get; set; }
        public DateTime endingDate { get; set; }
        public byte blocked { get; set; }
        public byte critical { get; set; }
    }
}
