#pragma checksum "E:\VCM Project\webpos\WebPOS\Views\Role\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2723bf029a0b004e98fdb3b679ad0ba5e8dd782d"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Role_Index), @"mvc.1.0.view", @"/Views/Role/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Role/Index.cshtml", typeof(AspNetCore.Views_Role_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "E:\VCM Project\webpos\WebPOS\Views\_ViewImports.cshtml"
using WebPOS;

#line default
#line hidden
#line 2 "E:\VCM Project\webpos\WebPOS\Views\_ViewImports.cshtml"
using WebPOS.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2723bf029a0b004e98fdb3b679ad0ba5e8dd782d", @"/Views/Role/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3b19a5705fd97bf9a14ec3088349bf9cd1907f4f", @"/Views/_ViewImports.cshtml")]
    public class Views_Role_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<WebPOS.Dto.Role>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", "text", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("RoleID"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("col-xs-10 col-sm-5"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("onchange", new global::Microsoft.AspNetCore.Html.HtmlString("UserCheck()"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("form-field-1"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Role", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Add", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_8 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("form-horizontal"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 2 "E:\VCM Project\webpos\WebPOS\Views\Role\Index.cshtml"
  
    ViewData["Title"] = "Phân quyền";
    ViewData["TitleActive"] = "Phân quyền";
    ViewData["TitleCurrent"] = "Phân quyền";
    ViewBag.Current = "UserRole";
    ViewData["Menu2"] = "active open";

#line default
#line hidden
            BeginContext(238, 2329, true);
            WriteLiteral(@"<div class=""main-content"">
    <div class=""main-content-inner"">
        <div class=""page-content"">

            <!-- PAGE CONTENT BEGINS -->
            <div class=""row"">
                <div class=""col-xs-12"">
                    <div class=""tabbable"">
                        <ul class=""nav nav-tabs"" id=""myTab"">
                            <li class=""active"">
                                <a data-toggle=""tab"" href=""#home"">
                                    <i class=""green ace-icon fa fa-home bigger-120""></i>
                                    Danh sách quyền truy cập
                                </a>
                            </li>

                            <li>
                                <a data-toggle=""tab"" href=""#messages"">
                                    <i class=""ace-icon glyphicon glyphicon-plus green bigger-110""></i>
                                    Thêm mới quyền truy cập

                                </a>
                            </li>



    ");
            WriteLiteral(@"                    </ul>

                        <div class=""tab-content"">
                            <div id=""home"" class=""tab-pane fade in active"">
                                <div class=""clearfix"">
                                    <div class=""pull-right tableTools-container""></div>
                                </div>
                                <div>
                                    <table id=""role-table"" class=""table table-striped table-bordered table-hover"" width=""100%"" cellspacing=""0"">
                                        <thead>
                                            <tr>
                                                <th>Role ID</th>
                                                <th>Role name</th>
                                                <th>Description</th>
                                                <th>Created Date</th>
                                                <th>Chức năng</th>
                                            </tr>
     ");
            WriteLiteral(@"                                   </thead>

                                    </table>

                                </div>
                            </div>



                            <div id=""messages"" class=""tab-pane fade "">
                                ");
            EndContext();
            BeginContext(2567, 2890, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "050f5b954324432e8561d035690c7de8", async() => {
                BeginContext(2650, 340, true);
                WriteLiteral(@"

                                    <div class=""form-group"">
                                        <label class=""col-sm-3 control-label no-padding-right"" for=""form-field-1"" style=""font-weight:bold;"">Mã phân quyền </label>

                                        <div class=""col-sm-9"">
                                            ");
                EndContext();
                BeginContext(2990, 107, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "315596c4ffe44d4197d4c02827a89ead", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_0.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                BeginWriteTagHelperAttribute();
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __tagHelperExecutionContext.AddHtmlAttribute("required", Html.Raw(__tagHelperStringValueBuffer), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.Minimized);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
#line 69 "E:\VCM Project\webpos\WebPOS\Views\Role\Index.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.RoleID);

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(3097, 770, true);
                WriteLiteral(@"
                                        </div>
                                    </div>
                                    <div class=""row"">
                                        <label class=""col-sm-2""></label>
                                        <div class=""col-sm-10"">
                                            <p id=""Status"" />
                                        </div>
                                    </div>
                                    <div class=""form-group"">
                                        <label class=""col-sm-3 control-label no-padding-right"" for=""form-field-1"" style=""font-weight:bold;""> Tên </label>

                                        <div class=""col-sm-9"">
                                            ");
                EndContext();
                BeginContext(3867, 92, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "196b51e510534d61b89fc17c07dc58ae", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_0.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
                BeginWriteTagHelperAttribute();
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __tagHelperExecutionContext.AddHtmlAttribute("required", Html.Raw(__tagHelperStringValueBuffer), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.Minimized);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
#line 82 "E:\VCM Project\webpos\WebPOS\Views\Role\Index.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.RoleName);

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(3959, 423, true);
                WriteLiteral(@"
                                        </div>
                                    </div>
                                    <div class=""form-group"">
                                        <label class=""col-sm-3 control-label no-padding-right"" for=""form-field-1"" style=""font-weight:bold;""> Mô tả </label>

                                        <div class=""col-sm-9"">
                                            ");
                EndContext();
                BeginContext(4382, 95, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "267479ccbb47422f92aea3f039d305e2", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_0.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
                BeginWriteTagHelperAttribute();
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __tagHelperExecutionContext.AddHtmlAttribute("required", Html.Raw(__tagHelperStringValueBuffer), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.Minimized);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
#line 89 "E:\VCM Project\webpos\WebPOS\Views\Role\Index.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.Description);

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4477, 973, true);
                WriteLiteral(@"
                                        </div>
                                    </div>
                                    <div class=""clearfix form-actions"">
                                        <div class=""col-md-offset-3 col-md-9"">
                                            <button class=""btn btn-info"" type=""submit"">
                                                <i class=""ace-icon fa fa-check bigger-110""></i>
                                                Lưu và tạo mới
                                            </button>
                                            <button class=""btn"" type=""reset"">
                                                <i class=""ace-icon fa fa-undo bigger-110""></i>
                                                Thiết lập lại
                                            </button>

                                        </div>
                                    </div>

                                ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_5.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_5);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Controller = (string)__tagHelperAttribute_6.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_6);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_7.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_7);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_8);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(5457, 278, true);
            WriteLiteral(@"
                            </div>
                        </div>

                    </div>


                </div>

                <!-- PAGE CONTENT ENDS -->

            </div><!-- /.page-content -->
        </div>
    </div>
</div><!-- /.main-content -->
");
            EndContext();
            DefineSection("scripts", async() => {
                BeginContext(5753, 152, true);
                WriteLiteral("\r\n    <!-- page specific plugin scripts -->\r\n\r\n    <script>\r\n        function UserCheck() {\r\n        $(\"#Status\").html(\"Kiểm tra...\");\r\n        $.post(\"");
                EndContext();
                BeginContext(5906, 47, false);
#line 127 "E:\VCM Project\webpos\WebPOS\Views\Role\Index.cshtml"
           Write(Url.Action("CheckUsernameAvailability", "Role"));

#line default
#line hidden
                EndContext();
                BeginContext(5953, 4049, true);
                WriteLiteral(@""",
            {
                userdata: $(""#RoleID"").val()
            },
        function (data) {
            if (data == 0) {
                $(""#Status"").html('<font color=""Green"">Bạn có thể sử dụng Role ID này</font>');
                $(""#RoleID"").css(""border-color"", ""Green"");

            }
            else {
                $(""#Status"").html('<font color=""Red"">Role ID này đã được sử dụng.</font>');
                $(""#RoleID"").css(""border-color"", ""Red"");
            }
        });
    }
        $(document).ready(function ()
        {
            if (!ace.vars['touch']) {
            $('.chosen-select').chosen({ allow_single_deselect: true });
            //resize the chosen on window resize

            $(window)
                .off('resize.chosen')
                .on('resize.chosen', function () {
                    $('.chosen-select').each(function () {
                        var $this = $(this);
                        $this.next().css({ 'width': $this.parent().widt");
                WriteLiteral(@"h() });
                    })
                }).trigger('resize.chosen');
            //resize chosen on sidebar collapse/expand
            $(document).on('settings.ace.chosen', function (e, event_name, event_val) {
                if (event_name != 'sidebar_collapsed') return;
                $('.chosen-select').each(function () {
                    var $this = $(this);
                    $this.next().css({ 'width': $this.parent().width() });
                })
            });


            $('#chosen-multiple-style .btn').on('click', function (e) {
                var target = $(this).find('input[type=radio]');
                var which = parseInt(target.val());
                if (which == 2) $('#form-field-select-4').addClass('tag-input-style');
                else $('#form-field-select-4').removeClass('tag-input-style');
            });
        }
            $(""#role-table"").DataTable({

                ""processing"": true, // for show progress bar
                ""serverSide""");
                WriteLiteral(@": true, // for process server side
                ""filter"": true, // this is for disable filter (search box)
                ""orderMulti"": false, // for disable multiple column at once
                ""ajax"": {
                    ""url"": ""Role/LoadData"",
                    ""type"": ""POST"",
                    ""datatype"": ""json""
                },

                ""columns"": [

                    { ""data"": ""RoleID"", ""name"": ""RoleID"", ""autoWidth"": true },
                    { ""data"": ""RoleName"", ""name"": ""RoleName"", ""autoWidth"": true },
                    { ""data"": ""Description"", ""name"": ""Description"", ""autoWidth"": true },
                    {
                        data: ""CreatedDate"", render: function (data, type, row) {
                            return moment(data).format(""DD-MM-YYYY"");
                        }
                    },

                    {
                        data: null, render: function (data, type, row) {
                            return ""<div class='btn");
                WriteLiteral(@"-group'><a class='btn btn-xs btn-info' href='/Role/Edit/"" + row.RoleID + ""'><i class='ace-icon fa fa-pencil-square-o bigger-120'></i></a><a href='#' class='btn btn-xs btn-danger' onclick=DeleteData('"" + row.RoleID + ""'); ><i class='ace-icon fa fa-trash-o bigger-120'></i></a></div>"";
                        }
                    },


                ]

            });

        });

      function DeleteData(RoleID)
        {
             bootbox.confirm({
						message: ""Bạn có chắc muốn xóa dòng này ...?"",
						buttons: {
						  confirm: {
							 label: ""Xác nhận"",
							 className: ""btn-primary btn-sm"",
						  },
						  cancel: {
							 label: ""Hủy"",
							 className: ""btn-sm"",
						  }
						},
						callback: function(result) {
                            if (result) { Delete(RoleID); }
                            else { }
						}
					  }
					);
        }


        function Delete(RoleID)
    {
        var url = '");
                EndContext();
                BeginContext(10003, 17, false);
#line 235 "E:\VCM Project\webpos\WebPOS\Views\Role\Index.cshtml"
              Write(Url.Content("~/"));

#line default
#line hidden
                EndContext();
                BeginContext(10020, 450, true);
                WriteLiteral(@"' + ""Role/Delete"";

            $.post(url, { ID: RoleID }, function (data)
                {
                    if (data)
                    {
                        oTable = $('#role-table').DataTable();
                        oTable.draw();
                    }
                    else
                    {
                        alert(""Có lỗi xảy ra!"");
                    }
                });
    }


    </script>

");
                EndContext();
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<WebPOS.Dto.Role> Html { get; private set; }
    }
}
#pragma warning restore 1591
