﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPOS.Models.ViewModels
{
    public class TransRawViewModels
    {
        public string StoreNo { get; set; }
        public string OrderNo { get; set; }
        public DateTime OrderDate { get; set; }
        public int Status { get; set; }
        public DateTime Crt_Date { get; set; }
        public DateTime ChgeDate { get; set; }
        public int StatusCode { get; set; }
        public string StatusName { get; set; }
        public int IsSuccess { get; set; }
        public string Message { get; set; }
    }
}

