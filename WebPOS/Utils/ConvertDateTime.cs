﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPOS.Utils
{
    public class ConvertDateTime
    {
        public static string GetDate2CharToday()
        {
            string _result = "";
            string _date = DateTime.Now.ToString();
            string[] _str; char pad = '0';
            if (!string.IsNullOrEmpty(_date))
            {
                _str = _date.ToString().Split(new char[] { '/' });
                _result = _str[2].Substring(0, 4) + _str[0].PadLeft(2, pad) + _str[1].PadLeft(2, pad);
            }
            return _result;
        }
        public static string Date2Char(string _date)
        {
            string _result = "";
            string[] _str;
            if (!string.IsNullOrEmpty(_date))
            {
                _str = _date.ToString().Split(new char[] { '-' });
                _result = _str[2] + _str[1] + _str[0];
            }

            return _result;
        }

        public static string DateCharReport(string _date)
        {
            string _result = "";
            string[] _str;
            char pad = '0';
            if (!string.IsNullOrEmpty(_date))
            {
                _str = _date.ToString().Split(new char[] { '/' });
                _result = _str[1].PadLeft(2, pad) + "-" + _str[0].PadLeft(2, pad) + "-" + _str[2].Substring(0, 4);
            }
            return _result;
        }

        public static string Date4Char(string _date)
        {
            string _result = "";
            string[] _str;
            char pad = '0';
            if (!string.IsNullOrEmpty(_date))
            {
                _str = _date.ToString().Split(new char[] { '/' });
                _result = _str[2].Substring(0, 4) + _str[0].PadLeft(2, pad) + _str[1].PadLeft(2, pad);
            }

            return _result;
        }
    }
}
