﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WebPOS.Models.Enum
{
    public enum LoginEnum
    {
        [Description(@"Đăng nhập thành công")]
        Success = 200,

        [Description(@"User không tồn tại")]
        UserError = 991,

        [Description(@"Mật khẩu không đúng")]
        PasswordError = 992,
    }
}
