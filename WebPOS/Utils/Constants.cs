﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPOS.Option
{
    public static class Constants
    {
        public static string DBConnectStringCentral()
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();
            return configuration["DbConnectString:DbConnectStringCentral"].ToString();
        }
    }
}
