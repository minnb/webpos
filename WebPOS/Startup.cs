﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebPOS.Models;
using WebPOS.Option;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.FileProviders;
using System.IO;

namespace WebPOS
{
    public class Startup
    {
        public static string WebApiBaseUrl { get; private set; }
        public static string WebApiBaseUrl_Token { get; private set; }
        public static string RestApiUrl_Token { get; private set; }
        public static string RestApiUrl { get; private set; }
        public static string GetEmergencyDataApiUrl { get; private set; }
        public static string GetEmergencyDataApiUrl_Token { get; private set; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
            WebApiBaseUrl = Configuration.GetSection("WebApiBaseUrl").Value;
            WebApiBaseUrl_Token = Configuration.GetSection("WebApiBaseUrl_Token").Value;       
            RestApiUrl = Configuration.GetSection("RestApiUrl").Value;
            RestApiUrl_Token = Configuration.GetSection("RestApiUrl_Token").Value;
            GetEmergencyDataApiUrl = Configuration.GetSection("GetEmergencyDataApiUrl").Value;
            GetEmergencyDataApiUrl_Token = Configuration.GetSection("GetEmergencyDataApiUrl_Token").Value;
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.ExpireTimeSpan = TimeSpan.FromDays(7);
                    options.LoginPath = "/Login";
                    //options.SlidingExpiration = true;
                });

            services.AddMvc(config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                                 .RequireAuthenticatedUser()
                                 .Build();
                config.Filters.Add(new AuthorizeFilter(policy));
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
            .AddJsonOptions(options =>
             {
                 options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                 // options.SerializerSettings.DateTimeZoneHandling = "MM/dd/yyyy HH:mm:ss";

             });
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            
            
            
            services.AddSession(options => { options.Cookie.IsEssential = true;
                options.IdleTimeout = TimeSpan.FromHours(24);
            });
            services.AddMemoryCache();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
           
                   }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseStatusCodePagesWithReExecute("/Error/{0}");

            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseCookiePolicy();
            app.UseStaticFiles();
            app.UseSession();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Login}/{action=Index}/{id?}");

            });
        }
        public static string GetBaseUrl()
        {
            return Startup.WebApiBaseUrl;
        }
        public static string GetTokenWebApi()
        {
            return Startup.WebApiBaseUrl_Token;
        }
        public static string GetRestApiUrl()
        {
            return Startup.RestApiUrl;
        }
        public static string GetTokenRestApi()
        {
            return Startup.RestApiUrl_Token;
        }
        
        public static string EmergencyDataApiUrl()
        {
            return Startup.GetEmergencyDataApiUrl;
        }
        public static string tokenEmergencyData()
        {
            return Startup.GetEmergencyDataApiUrl_Token;
        }
    }
}
