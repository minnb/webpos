﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebPOS.Dto;
using WebPOS.Models;
using WebPOS.Option;
using System.Data.SqlClient;
using System.Collections;
using System.Data;
using Microsoft.Extensions.Caching.Memory;
using WebPOS.Models.ViewModels;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WebPOS.Utils;
using WebPOS.Service;
using Microsoft.AspNetCore.Http;

namespace WebPOS.Controllers
{
    
    public class CouponController : Controller
    {
        private readonly ADO_NET _dbcon = new ADO_NET();
        private CentralDbContext db = new CentralDbContext();
        public CouponController()
        {
        }
        [HttpGet]
        [Authorize]
        public IActionResult Index(string CouponCode, string ItemName, string StartingDate, string EndingDate)
        {
            DataSet dataSet = new DataSet("TransRaw");
            Hashtable param = new Hashtable();
            


            try
            {
                if (string.IsNullOrEmpty(CouponCode) && string.IsNullOrEmpty(ItemName) && string.IsNullOrEmpty(StartingDate) && string.IsNullOrEmpty(EndingDate))
                {
                    param.Add("@CouponCode", "");
                    param.Add("@ItemName", "");
                    param.Add("@StartingDate", "");
                    param.Add("@EndingDate", "");
                    var connectStr = Constants.DBConnectStringCentral();
                    dataSet = _dbcon.ExecuteProcedure(connectStr, "SP_WEB_CHECK_CpnVch", param);
                }
                else if (string.IsNullOrEmpty(CouponCode) && string.IsNullOrEmpty(ItemName))
                {
                    param.Add("@CouponCode", "");
                    param.Add("@ItemName", "");
                    param.Add("@StartingDate", ConvertDateTime.Date2Char(StartingDate));
                    param.Add("@EndingDate", ConvertDateTime.Date2Char(EndingDate));
                    dataSet = _dbcon.ExecuteProcedure(Constants.DBConnectStringCentral(), "SP_WEB_CHECK_CpnVch", param);
                    ViewBag.EndingDate = EndingDate;
                    ViewBag.StartingDate = StartingDate;
                }
                else if (string.IsNullOrEmpty(CouponCode) && string.IsNullOrEmpty(ItemName) && string.IsNullOrEmpty(EndingDate))
                {
                    param.Add("@CouponCode", "");
                    param.Add("@ItemName", "");
                    param.Add("@StartingDate", ConvertDateTime.Date2Char(StartingDate));
                    param.Add("@EndingDate", "");
                    dataSet = _dbcon.ExecuteProcedure(Constants.DBConnectStringCentral(), "SP_WEB_CHECK_CpnVch", param);
                    
                    ViewBag.StartingDate = StartingDate;
                }
                else if (string.IsNullOrEmpty(CouponCode) && string.IsNullOrEmpty(ItemName) && string.IsNullOrEmpty(StartingDate))
                {
                    param.Add("@CouponCode", "");
                    param.Add("@ItemName", "");
                    param.Add("@EndingDate", ConvertDateTime.Date2Char(EndingDate));
                    param.Add("@StartingDate", "");
                    dataSet = _dbcon.ExecuteProcedure(Constants.DBConnectStringCentral(), "SP_WEB_CHECK_CpnVch", param);

                    ViewBag.EndingDate = EndingDate;
                }
                else if (string.IsNullOrEmpty(CouponCode))
                {
                    param.Add("@CouponCode", "");
                    param.Add("@ItemName", ItemName);
                    param.Add("@StartingDate", ConvertDateTime.Date2Char(StartingDate));
                    param.Add("@EndingDate", ConvertDateTime.Date2Char(EndingDate));
                    dataSet = _dbcon.ExecuteProcedure(Constants.DBConnectStringCentral(), "SP_WEB_CHECK_CpnVch", param);
                    ViewBag.From_Date = StartingDate;
                    ViewBag.EndingDate = EndingDate;
                    ViewBag.ItemName = ItemName;
                }
                else if (string.IsNullOrEmpty(ItemName))
                {
                    param.Add("@CouponCode", CouponCode);
                    param.Add("@ItemName", "");
                    param.Add("@StartingDate", ConvertDateTime.Date2Char(StartingDate));
                    param.Add("@EndingDate", ConvertDateTime.Date2Char(EndingDate));
                    dataSet = _dbcon.ExecuteProcedure(Constants.DBConnectStringCentral(), "SP_WEB_CHECK_CpnVch", param);
                    ViewBag.StartingDate = StartingDate;
                    ViewBag.EndingDate = EndingDate;
                    ViewBag.CouponCode = CouponCode;
                }
                else if (string.IsNullOrEmpty(StartingDate))
                {
                    param.Add("@CouponCode", CouponCode);
                    param.Add("@ItemName", ItemName);
                    param.Add("@StartingDate", "");
                    param.Add("@EndingDate", ConvertDateTime.Date2Char(EndingDate));
                    dataSet = _dbcon.ExecuteProcedure(Constants.DBConnectStringCentral(), "SP_WEB_CHECK_CpnVch", param);
                    ViewBag.EndingDate = EndingDate;
                    ViewBag.ItemName = ItemName;
                    ViewBag.CouponCode = CouponCode;
                }
                else if (string.IsNullOrEmpty(EndingDate))
                {
                    param.Add("@CouponCode", CouponCode);
                    param.Add("@ItemName", ItemName);
                    param.Add("@EndingDate", "");
                    param.Add("@StartingDate", ConvertDateTime.Date2Char(StartingDate));
                    dataSet = _dbcon.ExecuteProcedure(Constants.DBConnectStringCentral(), "SP_WEB_CHECK_CpnVch", param);
                    ViewBag.StartingDate = StartingDate;
                    ViewBag.ItemName = ItemName;
                    ViewBag.CouponCode = CouponCode;
                }
                else
                {
                    param.Add("@CouponCode", CouponCode);
                    param.Add("@ItemName", ItemName);
                    param.Add("@StartingDate", ConvertDateTime.Date2Char(StartingDate));
                    param.Add("@EndingDate", ConvertDateTime.Date2Char(EndingDate));
                    dataSet = _dbcon.ExecuteProcedure(Constants.DBConnectStringCentral(), "SP_WEB_CHECK_CpnVch", param);
                    ViewBag.StartingDate = StartingDate;
                    ViewBag.EndingDate = EndingDate;
                    ViewBag.CouponCode = CouponCode;
                    ViewBag.ItemName = ItemName;
                }
            }
            catch
            {

            }

            return View(dataSet);


        }

        [Authorize]
        [HttpPost]
        public IActionResult PostListTransRawAsync(string CouponCode, string ItemName, string StartingDate, string EndingDate)
        {
            return View();
        }
        [Authorize]
        [HttpGet]
        public IActionResult ViewDetail(string id)
        {

            ViewBag.sale = db.CpnVchBOMHeader.Where(st => st.ItemNo == id).FirstOrDefault();
            var stu = db.CpnVchBOMLine.Where(st => st.ItemNo == id);

            //Tạo list
            //List<Student> stuList = stu.ToList();
            ViewBag.products = stu.ToList();
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "Sale");
                }

                return View("ViewDetail");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}