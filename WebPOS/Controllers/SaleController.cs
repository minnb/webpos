﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebPOS.Dto;
using WebPOS.Models;
using WebPOS.Option;
using System.Data.SqlClient;
using System.Collections;
using System.Data;
using Microsoft.Extensions.Caching.Memory;
using WebPOS.Models.ViewModels;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WebPOS.Utils;
using WebPOS.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebPOS.Controllers
{
    
    public class SaleController : Controller
    {
        private readonly ADO_NET _dbcon = new ADO_NET();
        private CentralDbContext db = new CentralDbContext();
        public SaleController()
        {
        }
        [Authorize]
        [HttpGet]
        public IActionResult Index(string StoreNo, string POSTerminalNo, string From_Date, string To_Date)
        {
            IEnumerable<SelectListItem> items = db.Store.Select(c => new SelectListItem
            {
                Value = c.No,
                Text = c.No + " + " + c.Name,
                Selected = (c.No == StoreNo) ? true : false

            }); 
            ViewBag.StoreNo = items;
            string storeno = items.FirstOrDefault().Value;

            DataSet dataSet = new DataSet("TransRaw");
            Hashtable param = new Hashtable();
            string today = DateTime.Today.ToString("yyyyMMdd");
            string yesterday = DateTime.Today.AddDays(-1).ToString("yyyyMMdd");
            ViewBag.From_Date = DateTime.Today.AddDays(-1).ToString("dd-MM-yyyy");
            ViewBag.To_Date = DateTime.Today.ToString("dd-MM-yyyy");
           // From_Date 

            try
            {
                if (string.IsNullOrEmpty(StoreNo) && string.IsNullOrEmpty(POSTerminalNo) && string.IsNullOrEmpty(From_Date) && string.IsNullOrEmpty(To_Date))
                {
                    param.Add("@StoreNo", storeno);
                    param.Add("@POSTerminalNo", "");
                    param.Add("@From_Date", yesterday);
                    param.Add("@To_Date", today);
                    var connectStr = Constants.DBConnectStringCentral();
                    dataSet = _dbcon.ExecuteProcedure(connectStr, "SP_WEB_CHECK_TRANS", param);
                }
                else if (string.IsNullOrEmpty(POSTerminalNo) && string.IsNullOrEmpty(From_Date) && string.IsNullOrEmpty(To_Date))
                {
                    param.Add("@StoreNo", StoreNo);
                    param.Add("@POSTerminalNo", "");
                    param.Add("@From_Date", yesterday);
                    param.Add("@To_Date", today);
                    dataSet = _dbcon.ExecuteProcedure(Constants.DBConnectStringCentral(), "SP_WEB_CHECK_TRANS", param);
                    //ViewBag.From_Date = From_Date;
                   // ViewBag.To_Date = To_Date;
                }
                else if (string.IsNullOrEmpty(StoreNo) && string.IsNullOrEmpty(POSTerminalNo))
                {
                    param.Add("@StoreNo", "");
                    param.Add("@POSTerminalNo", "");
                    param.Add("@From_Date", ConvertDateTime.Date2Char(From_Date));
                    param.Add("@To_Date", ConvertDateTime.Date2Char(To_Date));
                    dataSet = _dbcon.ExecuteProcedure(Constants.DBConnectStringCentral(), "SP_WEB_CHECK_TRANS", param);
                    ViewBag.From_Date = From_Date;
                    ViewBag.To_Date = To_Date;
                }
                else if (string.IsNullOrEmpty(StoreNo))
                {
                    param.Add("@StoreNo", "");
                    param.Add("@POSTerminalNo", POSTerminalNo);
                    param.Add("@From_Date", ConvertDateTime.Date2Char(From_Date));
                    param.Add("@To_Date", ConvertDateTime.Date2Char(To_Date));
                    dataSet = _dbcon.ExecuteProcedure(Constants.DBConnectStringCentral(), "SP_WEB_CHECK_TRANS", param);
                    ViewBag.From_Date = From_Date;
                    ViewBag.To_Date = To_Date;
                    ViewBag.POSTerminalNo = POSTerminalNo;
                }
                else if (string.IsNullOrEmpty(POSTerminalNo))
                {
                    param.Add("@StoreNo", StoreNo);
                    param.Add("@POSTerminalNo", "");
                    param.Add("@From_Date", ConvertDateTime.Date2Char(From_Date));
                    param.Add("@To_Date", ConvertDateTime.Date2Char(To_Date));
                    dataSet = _dbcon.ExecuteProcedure(Constants.DBConnectStringCentral(), "SP_WEB_CHECK_TRANS", param);
                    ViewBag.From_Date = From_Date;
                    ViewBag.To_Date = To_Date;
                    //ViewBag.StoreNo = StoreNo;
                }
                else
                {
                    param.Add("@StoreNo", StoreNo);
                    param.Add("@POSTerminalNo", POSTerminalNo);
                    param.Add("@From_Date", ConvertDateTime.Date2Char(From_Date));
                    param.Add("@To_Date", ConvertDateTime.Date2Char(To_Date));
                    dataSet = _dbcon.ExecuteProcedure(Constants.DBConnectStringCentral(), "SP_WEB_CHECK_TRANS", param);
                    ViewBag.From_Date = From_Date;
                    ViewBag.To_Date = To_Date;
                    ViewBag.POSTerminalNo = POSTerminalNo;
                    //ViewBag.StoreNo = StoreNo;
                }
            }
            catch
            {

            }
           // students.Add(dataSet);
            return View(dataSet);


        }

        [Authorize]
        [HttpPost]
        public IActionResult PostListTransRawAsync(string StoreNo, string POSTerminalNo, string From_Date, string To_Date)
        {
            return View();
        }
        [HttpGet]
        [Authorize]
        public IActionResult ViewDetail(string id)
        {
            
             ViewBag.sale = db.TransHeader.Where(st => st.OrderNo == id).FirstOrDefault();
            IEnumerable<TransLine> stu = db.TransLine.Where(st => st.DocumentNo == id);

            //Tạo list
            //List<Student> stuList = stu.ToList();
            ViewBag.products = stu.ToList();
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "Sale");
                }

                return View("ViewDetail");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}