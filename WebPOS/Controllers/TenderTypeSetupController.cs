﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebPOS.Dto;
using WebPOS.Models;
using WebPOS.Option;

namespace WebPOS.Controllers
{

    
    public class TenderTypeSetupController : Controller
    {
        private CentralDbContext db = new CentralDbContext();

        [Authorize]
        public IActionResult Index()
        {
            //ViewBag.stores = db.Store.FromSql("select l.* from Store l left join TenderType r on r.StoreNo = l.No where r.StoreNo is null").ToList();
            IEnumerable<SelectListItem> itemss = db.Store.FromSql("select * from Store where exists (select StoreNo from TenderType where TenderType.StoreNo = Store.No)").Select(c => new SelectListItem
            {
                Value = c.No,
                Text = c.No + " + " + c.Name

            }); ;
            ViewBag.StoreNo1 = itemss;
            IEnumerable<SelectListItem> items = db.Store.FromSql("select l.* from Store l left join TenderType r on r.StoreNo = l.No where r.StoreNo is null").Select(c => new SelectListItem
            {
                Value = c.No,
                Text = c.No + " + " + c.Name

            }); ;
            ViewBag.StoreNo2 = items;
            return View();
        }
        [Authorize]
        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skiping number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction ( asc ,desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();

                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Customer data  
                var customerData = (from tempcustomer in db.TenderTypeSetup
                                    select tempcustomer);

                //Sorting  
                // if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                // {
                // customerData = customerData.OrderBy(sortColumn + " " + sortColumnDirection);
                // }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    customerData = customerData.Where(m => m.Code == searchValue || m.Description == searchValue);
                }

                //total number of rows count   
                recordsTotal = customerData.Count();
                //Paging   
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception)
            {
                throw;
            }

        }
        [Authorize]
        public IActionResult LoadData2()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skiping number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction ( asc ,desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();

                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Customer data  
                var customerData = (from tempcustomer in db.TenderType
                                    select tempcustomer);

                //Sorting  
                // if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                // {
                // customerData = customerData.OrderBy(sortColumn + " " + sortColumnDirection);
                // }
                // Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    customerData = customerData.Where(m => m.StoreNo == searchValue);
                }

                //total number of rows count   
                recordsTotal = customerData.Count();
                //Paging   
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception)
            {
                throw;
            }

        }
        [HttpPost]
        [Authorize]
        public ActionResult AddTenderType(IFormCollection request)
        {
            string StoreNo1 = request["StoreNo1"].ToString().Trim();
            string StoreNo2 = request["StoreNo2"].ToString().Trim();
            var tendertype = db.TenderType.Where((m => m.StoreNo == StoreNo1)).ToList();
            int total = tendertype.Count();
            
            if (total > 0)
            {
                for (int i = 0; i < total; i++ )
                {
                    TenderType tendertypenew = new TenderType
                    {
                        StoreNo = StoreNo2,
                        Code = tendertype[i].Code,
                        Description = tendertype[i].Description,
                        Function = tendertype[i].Function,
                        ChangeTenderCode = tendertype[i].ChangeTenderCode,
                        Rounding = tendertype[i].Rounding,
                        RoundingTo = tendertype[i].RoundingTo,
                        MinAmountEntered = tendertype[i].MinAmountEntered,
                        MaxAmountEntered = tendertype[i].MaxAmountEntered,
                        MinAmountAllowed = tendertype[i].MinAmountAllowed,
                        MaxAmountAllowed = tendertype[i].MaxAmountAllowed,
                        MayBeUsed = tendertype[i].MayBeUsed,
                        ManagerKeyControl = tendertype[i].ManagerKeyControl,
                        KeyboardEntryAllowed = tendertype[i].KeyboardEntryAllowed,
                        OvertenderAllowed = tendertype[i].OvertenderAllowed,
                        OvertenderMaxAmt = tendertype[i].OvertenderMaxAmt,
                        DrawerOpens = tendertype[i].DrawerOpens,
                        CardAccountNo = tendertype[i].CardAccountNo,
                        AskForDate = tendertype[i].AskForDate,
                        ForeignCurrency = tendertype[i].ForeignCurrency,
                        FloatAllowed = tendertype[i].FloatAllowed,
                        BankAccountType = tendertype[i].BankAccountType,
                        BankAccountNo = tendertype[i].BankAccountNo,
                        BankAccountName = tendertype[i].BankAccountName,
                        ReturnVoucher = tendertype[i].ReturnVoucher,
                        AccountNo = tendertype[i].AccountNo

                    };

                    db.TenderType.Add(tendertypenew);
                    db.SaveChanges();
                }
            }

    

            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize]
        public IActionResult Edit(string id)
        {
            // ViewBag.stores = db.Store.ToList();
            ViewBag.tendertype = db.TenderTypeSetup.Find(id);
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "TenderTypeSetup");
                }

                return View("Edit");
            }
            catch (Exception)
            {
                throw;
            }
        }
        [HttpPost]
        [Authorize]
        public IActionResult Edit(TenderTypeSetup obj)
        {
            TenderTypeSetup existing = db.TenderTypeSetup.Find(obj.Code);
            existing.Description = obj.Description;
            existing.CurrencyCode = obj.CurrencyCode;
            existing.Caption = obj.Caption;
            db.Entry(existing).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet]
        [Authorize]
        public IActionResult ViewDetail(string id)
        {
            //ViewBag.stores = db.Store.ToList();
            ViewBag.tendertype = db.TenderTypeSetup.Find(id);
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "TenderTypeSetup");
                }

                return View("ViewDetail");
            }
            catch (Exception)
            {
                throw;
            }
        }


        [HttpPost]
        [Authorize]
        public IActionResult Delete(string id)
        {
            db.TenderTypeSetup.Remove(db.TenderTypeSetup.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");

        }


        [HttpGet]
        [Authorize]
        public IActionResult Add()
        {
           // ViewBag.stores = db.Store.ToList();
            return View();
        }
        public JsonResult CheckUsernameAvailability(string userdata)
        {
            System.Threading.Thread.Sleep(200);
            var SeachData = db.TenderTypeSetup.Where(x => x.Code == userdata).SingleOrDefault();
            if (SeachData != null)
            {
                return Json(1);
            }
            else
            {
                return Json(0);
            }

        }
        [HttpPost]
        [Authorize]
        public IActionResult Add(TenderTypeSetup obj)
        {
            //  POSTerminal existing = db.POSTerminal.Find(obj.No);
            obj.Caption = " ";
            obj.RefKey1 = " ";
            obj.RefKey2 = " ";
            obj.CurrencyCode = " ";
           
            db.TenderTypeSetup.Add(obj);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


    }
}