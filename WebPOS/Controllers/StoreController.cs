﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using WebPOS.Dto;
using WebPOS.Models;
using WebPOS.Option;

namespace WebPOS.Controllers
{
    
    public class StoreController : Controller
    {
        private CentralDbContext db = new CentralDbContext();
        //private CentralDbContext _context = new CentralDbContext();
        //public POSTerminalController(CentralDbContext context)
        //{
        //_context = context;
        //}
        // GET: /<controller>/  
        [Authorize]
        public IActionResult Index()
        {
            return View();
        }
        [Authorize]
        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skiping number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction ( asc ,desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();

                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Customer data  
                var customerData = (from tempcustomer in db.Store
                                    select tempcustomer);

                //Sorting  
                // if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                // {
                // customerData = customerData.OrderBy(sortColumn + " " + sortColumnDirection);
                // }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    customerData = customerData.Where(m => m.No == searchValue || m.Name == searchValue || m.Address == searchValue);
                }

                //total number of rows count   
                recordsTotal = customerData.Count();
                //Paging   
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception)
            {
                throw;
            }

        }

        [HttpGet]
        [Authorize]
        public IActionResult Edit(string id)
        {
            //ViewBag.stores = db.Store.ToList();
            ViewBag.store = db.Store.Find(id);
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "Store");
                }

                return View("Edit");
            }
            catch (Exception)
            {
                throw;
            }
        }
        [HttpPost]
        [Authorize]
        public IActionResult Edit(IFormCollection obj)
        {
            Store existing = db.Store.Find(obj["No"]);
            //existing.No = obj.No;
            //existing.Name = obj.Name;
            existing.Address = obj["Address"];
            existing.City = obj["City"];
            existing.TaxCode = obj["TaxCode"];
            existing.PhoneNo = obj["PhoneNo"];
            existing.LastDateModified = DateTime.Now;
            string checkboxvalue = obj["ClosingMethod"];
           if (checkboxvalue == "true") { existing.ClosingMethod = 1; }
            else { existing.ClosingMethod = 0; }
            db.Entry(existing).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize]
        public IActionResult ViewDetail(string id)
        {
            //ViewBag.stores = db.Store.ToList();
            ViewBag.posterminal = db.Store.Find(id);
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "Store");
                }

                return View("ViewDetail");
            }
            catch (Exception)
            {
                throw;
            }
        }
        [Authorize]
        public async Task<IActionResult> PushMasterData(string StoreNo, string Type)
        {
            string EmergencyDataApiUrl = Startup.EmergencyDataApiUrl();
            string tokenEmergencyData = Startup.tokenEmergencyData();
            string storeno = "";
            string type = "";
            if (StoreNo != null)
            {
                storeno = "&_storeNo=" + StoreNo;
            }

            if (Type != null)
            {
                type = "&_type=" + Type;
            }
            string baseURL = EmergencyDataApiUrl + $"v1/ITS/GetEmergencyData?_token=" + tokenEmergencyData + type + storeno;
            try
            {
                //Now we will have our using directives which would have a HttpClient 
                using (HttpClient client = new HttpClient())
                {
                    //Now get your response from the client from get request to baseurl.
                    //Use the await keyword since the get request is asynchronous, and want it run before next asychronous operation.
                    using (HttpResponseMessage res = await client.GetAsync(baseURL))
                    {
                        //Now we will retrieve content from our response, which would be HttpContent, retrieve from the response Content property.
                        using (HttpContent content = res.Content)
                        {
                            var dataObj = await content.ReadAsStringAsync();
                            
                            dynamic json = JValue.Parse(dataObj);

                            // values require casting
                            string code = json.code;
                            int quantity = json.quantity;
                            if (code == "200" && quantity > 0)
                            {
                                //ViewBag.MessageData = "Đẩy thành công";
                                return Json(new { success = true, responseText = "Đẩy dữ liệu thành công" });
                            }
                            else { return Json(new { success = false, responseText = "Đẩy dữ liệu thất bại" }); }
                           
                           
                        }
                    }
                }
            }
            catch
            {
                throw;
            }

            
        }




    }
}