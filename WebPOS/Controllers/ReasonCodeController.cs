﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebPOS.Dto;
using WebPOS.Option;

namespace WebPOS.Controllers
{

    
    public class ReasonCodeController : Controller
    {
        private CentralDbContext db = new CentralDbContext();
        //private CentralDbContext _context = new CentralDbContext();
        //public POSTerminalController(CentralDbContext context)
        //{
        //_context = context;
        //}
        // GET: /<controller>/  
        [Authorize]
        public IActionResult Index()
        {
            return View();
        }
        [Authorize]
        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skiping number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction ( asc ,desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();

                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Customer data  
                var customerData = (from tempcustomer in db.ReasonCode
                                    select tempcustomer);

                //Sorting  
                // if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                // {
                // customerData = customerData.OrderBy(sortColumn + " " + sortColumnDirection);
                // }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    customerData = customerData.Where(m => m.Code == searchValue || m.Description == searchValue || m.Group == searchValue);
                }

                //total number of rows count   
                recordsTotal = customerData.Count();
                //Paging   
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception)
            {
                throw;
            }

        }
        [HttpGet]
        [Authorize]
        public IActionResult Edit(string id)
        {
            // ViewBag.stores = db.Store.ToList();
            ViewBag.reason = db.ReasonCode.Find(id);
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "ReasonCode");
                }

                return View("Edit");
            }
            catch (Exception)
            {
                throw;
            }
        }
        [HttpPost]
        [Authorize]
        public IActionResult Edit(ReasonCode obj)
        {
            ReasonCode existing = db.ReasonCode.Find(obj.Code);
            existing.Description = obj.Description;
            existing.Group = obj.Group;
           
            db.Entry(existing).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet]
        [Authorize]
        public IActionResult ViewDetail(string id)
        {
            //ViewBag.stores = db.Store.ToList();
            ViewBag.posterminal = db.ReasonCode.Find(id);
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "ReasonCode");
                }

                return View("ViewDetail");
            }
            catch (Exception)
            {
                throw;
            }
        }
        

        [HttpPost]
        [Authorize]
        public IActionResult Delete(string id)
        {
            db.ReasonCode.Remove(db.ReasonCode.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        [HttpGet]
        [Authorize]
        public IActionResult Add()
        {
           // ViewBag.stores = db.Store.ToList();
            return View();
        }
        public JsonResult CheckUsernameAvailability(string userdata)
        {
            System.Threading.Thread.Sleep(200);
            var SeachData = db.ReasonCode.Where(x => x.Code == userdata).SingleOrDefault();
            if (SeachData != null)
            {
                return Json(1);
            }
            else
            {
                return Json(0);
            }

        }
        [HttpPost]
        [Authorize]
        public IActionResult Add(ReasonCode obj)
        {
            //  POSTerminal existing = db.POSTerminal.Find(obj.No);
            var SeachData = db.ReasonCode.Where(x => x.Code == obj.Code).SingleOrDefault();
            if (SeachData == null)
            {
                db.ReasonCode.Add(obj);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else {
                ViewBag.Message = "Mã đã được sử dụng";
                return RedirectToAction("Index");
            }
        }


    }
}