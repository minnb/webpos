﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebPOS.Dto;
using WebPOS.Models;
using WebPOS.Option;

namespace WebPOS.Controllers
{
    [Authorize]
    public class FunctionController : Controller
    {
        private CentralDbContext db = new CentralDbContext();
        //private CentralDbContext _context = new CentralDbContext();
        //public POSTerminalController(CentralDbContext context)
        //{
        //_context = context;
        //}
        // GET: /<controller>/  
        public IActionResult Index()
        {
            ViewBag.stores = db.Function.ToList();
            return View();
        }

        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skiping number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction ( asc ,desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();

                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Customer data  
                var customerData = (from tempcustomer in db.Function
                                    select tempcustomer);

                //Sorting  
                // if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                // {
                // customerData = customerData.OrderBy(sortColumn + " " + sortColumnDirection);
                // }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    customerData = customerData.Where(m => m.Name == searchValue);
                }

                //total number of rows count   
                recordsTotal = customerData.Count();
                //Paging   
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception)
            {
                throw;
            }

        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            ViewBag.stores = db.Function.ToList();
            ViewBag.function = db.Function.Find(id);
            var storeno = db.Function.Where(x => x.Id == id).SingleOrDefault();
            IEnumerable<SelectListItem> items = db.Function.Select(c => new SelectListItem
            {
                Value = Convert.ToString(c.Id),
                Text = c.Name,
                Selected = (c.Id == storeno.ParentId) ? true : false
            }); ;
            ViewBag.stores = items;
            return View();
        }
        [HttpPost]
        public IActionResult Edit(Function obj)
        {
            Function existing = db.Function.Find(obj.Id);
            existing.Name = obj.Name;
            existing.Url = obj.Url;
            existing.CssClass = obj.CssClass;
            existing.ParentId = obj.ParentId;
            existing.SortOrder = obj.SortOrder;
            // existing.LastDateModified = DateTime.Now;
            db.Entry(existing).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }



        [HttpPost]
        public IActionResult Delete(int id)
        {
            // string newid = @"VINGROUP\";
            //string newidnew = newid + id;
            db.Function.Remove(db.Function.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");

        }

        [HttpGet]
        public IActionResult Add()
        {
            ViewBag.stores = db.Function.ToList();
            return View();
        }
        public JsonResult CheckUsernameAvailability(int userdata)
        {
            System.Threading.Thread.Sleep(200);
            
            var SeachData = db.Function.Where(x => x.Id == userdata).SingleOrDefault();
            if (SeachData != null)
            {
                return Json(1);
            }
            else
            {
                return Json(0);
            }

        }
        [HttpPost]
        public IActionResult Add(Function obj)
        {
            

            db.Function.Add(obj);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}