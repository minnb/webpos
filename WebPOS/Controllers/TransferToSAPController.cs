﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebPOS.Option;
using System.Data;
using WebPOS.Utils;
using Microsoft.AspNetCore.Http;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebPOS.Controllers
{

    public class TransferToSAPController : Controller
    {
        private CentralDbContext db = new CentralDbContext();
        [Authorize]
        public IActionResult Index()
        {
            IEnumerable<SelectListItem> items = db.Store.Select(c => new SelectListItem
            {
                Value = c.No,
                Text = c.No + " + " + c.Name

            }); ;
            ViewBag.StoreNo = items;
            string today = DateTime.Today.ToString("dd-MM-yyyy");
            string yesterday = DateTime.Today.AddDays(-1).ToString("dd-MM-yyyy");
            ViewBag.From_Date = yesterday;
            ViewBag.To_Date = today;
            return View();
        }

        [Authorize]
        public async Task<IActionResult> LoadData(string From_Date, string To_Date, string OrderNo, string StoreNo)
        {
            string BaseUrl = Startup.WebApiBaseUrl();
            string Token = Startup.GetTokenWebApi();


            string fromdate = "&_fromDate=" + DateTime.Today.AddDays(-1).ToString("yyyyMMdd");
            string todate = "&_toDate=" + DateTime.Today.ToString("yyyyMMdd");
            string orderno = "";
            string storeno = "";
            //DataSet dataSet = new DataSet("TransRaw");
            // Hashtable param = new Hashtable();
            if (From_Date != null)
            {

                fromdate = "&_fromDate=" + ConvertDateTime.Date2Char(From_Date);
            }
            if (To_Date != null)
            {
                todate = "&_toDate=" + ConvertDateTime.Date2Char(To_Date);
            }
            if (OrderNo != null)
            {
                orderno = "&_orderNo=" + OrderNo;
            }
            if (StoreNo != null)
            {
                storeno = "&_storeNo=" + StoreNo;
            }

            string baseURL = BaseUrl + $"v1/WebApi/GetCheckTransStatusToSAP?_token=" + Token + orderno + storeno + fromdate + todate;

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //Now get your response from the client from get request to baseurl.
                    //Use the await keyword since the get request is asynchronous, and want it run before next asychronous operation.
                    using (HttpResponseMessage res = await client.GetAsync(baseURL))
                    {
                        //Now we will retrieve content from our response, which would be HttpContent, retrieve from the response Content property.
                        using (HttpContent content = res.Content)
                        {
                            var dataObj = await content.ReadAsStringAsync();
                            if (dataObj != "null")
                            {

                                var array = JArray.Parse(dataObj);

                                var data = array.Select(emp => emp).ToList();


                                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                                // Skiping number of Rows count  
                                var start = Request.Form["start"].FirstOrDefault();
                                // Paging Length 10,20  
                                var length = Request.Form["length"].FirstOrDefault();
                                // Sort Column Name  
                                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                                // Sort Column Direction ( asc ,desc)  
                                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                                // Search Value from (Search box)  
                                // var searchValue = Request.Form["search[value]"].FirstOrDefault();

                                //Paging Size (10,20,50,100)  
                                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                                int skip = start != null ? Convert.ToInt32(start) : 0;
                                int recordsTotal = 0;

                                //total number of rows count   
                                recordsTotal = data.Count();
                                //Paging   
                                var data2 = data.Skip(skip).Take(pageSize).ToList();

                                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data2 });


                            }
                            else
                            {
                                return Json(new { draw = 0, recordsTotal = 0, recordsFiltered = 0, data = "" });
                            }


                        }


                    }
                }
            }
            catch
            {
                throw;
            }
        }


    }
}
