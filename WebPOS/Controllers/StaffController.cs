﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebPOS.Dto;
using WebPOS.Models;
using WebPOS.Option;

namespace WebPOS.Controllers
{
    
    public class StaffController : Controller
    {
        private CentralDbContext db = new CentralDbContext();
        //private CentralDbContext _context = new CentralDbContext();
        //public POSTerminalController(CentralDbContext context)
        //{
        //_context = context;
        //}
        // GET: /<controller>/  
        [Authorize]
        public IActionResult Index()
        {
            IEnumerable<SelectListItem> items = db.Store.Select(c => new SelectListItem
            {
                Value = c.No,
                Text = c.No + " + " + c.Name

            }); ;
            ViewBag.StoreNo = items;
            return View();
        }
        [HttpPost]
        [Authorize]
        public IActionResult AddRoleUser(IFormCollection request)
        {
            string userid = request["UserName"].ToString().Trim();
            //UserRole rolenew = new UserRole
            // {
            string roleid = request["RoleID"].ToString().Trim();

            var temp = db.UserRole.Where(s => s.UserID == userid);
            foreach (var item in temp)
            {
                db.UserRole.Remove(item);
            }

            UserRole rolenew = new UserRole
            {
                UserID = userid,
                RoleID = roleid,
                Status = "1"

            };
            db.UserRole.Add(rolenew);
            db.SaveChanges();

            return RedirectToAction("Index");
        }
        [HttpGet]
        [Authorize]
        public IActionResult ViewDetail(string id)
        {
            //ViewBag.stores = db.Store.ToList();
            ViewBag.posterminal = db.Staff.Find(id);
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "Staff");
                }

                return View("ViewDetail");
            }
            catch (Exception)
            {
                throw;
            }
        }
        [Authorize]
        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skiping number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction ( asc ,desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();

                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Customer data  
                var customerData = (from tempcustomer in db.Staff
                                    select tempcustomer);

                //Sorting  
                // if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                // {
                // customerData = customerData.OrderBy(sortColumn + " " + sortColumnDirection);
                // }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    customerData = customerData.Where(m => m.ID == searchValue || m.FirstName == searchValue || m.LastName == searchValue);
                }

                //total number of rows count   
                recordsTotal = customerData.Count();
                //Paging   
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception)
            {
                throw;
            }

        }

        [HttpGet]
        [Authorize]
        public IActionResult Edit(string id)
        {
            
            ViewBag.staff = db.Staff.Find(id);
            ViewBag.role = db.Role.ToList();
            var storeno = db.Staff.Where(x => x.ID == id).SingleOrDefault(); 
            IEnumerable<SelectListItem> items = db.Store.Select(c => new SelectListItem
            {
                Value = c.No,
                Text = c.No + " + " + c.Name,
                Selected = (c.No == storeno.StoreNo) ? true : false
            }); ;
            ViewBag.StoreNo = items;
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "Staff");
                }

                return View("Edit");
            }
            catch (Exception)
            {
                throw;
            }
        }
        [HttpPost]
        [Authorize]
        public IActionResult Edit(IFormCollection obj)
        {
            Staff existing = db.Staff.Find(obj["ID"]);
            existing.FirstName = obj["FirstName"];
            existing.StoreNo = obj["StoreNo"];
            existing.DateToBeBlocked = Convert.ToDateTime((obj["DateToBeBlocked"]));
            existing.LastDateModified = DateTime.Now;
            string checkboxvalue = obj["Blocked"];
            if (checkboxvalue == "true") { existing.Blocked = 1; }
            else { existing.Blocked = 0; }
            db.Entry(existing).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet]
        [Authorize]
        public IActionResult Add()
        {
            IEnumerable<SelectListItem> items = db.Store.Select(c => new SelectListItem
            {
                Value = c.No,
                Text = c.No + " + " + c.Name

            }); ;
            ViewBag.StoreNo = items;
            return View();
        }
        public JsonResult CheckUsernameAvailability(string userdata)
        {
            System.Threading.Thread.Sleep(200);
            var SeachData = db.Staff.Where(x => x.ID == userdata).SingleOrDefault();
            if (SeachData != null)
            {
                return Json(1);
            }
            else
            {
                return Json(0);
            }

        }
        [HttpPost]
        [Authorize]
        public IActionResult Add(Staff obj)
        {
            var SeachData = db.Staff.Where(x => x.ID == obj.ID).SingleOrDefault();
            if (SeachData == null)
            {
                obj.LastName = " ";
                obj.NameOnReceipt = " ";
                obj.Address = " ";
                obj.City = " ";
                obj.PostCode = " ";
                obj.HomePhoneNo = " ";
                obj.WorkPhoneNo = " ";
                obj.PayrollNo = " ";
                obj.SalesPerson = " ";
                obj.PermissionGroup = " ";
                obj.Language = " ";
                obj.InventoryMainMenu = " ";
                obj.POSStyleProfile = " ";
                obj.POSMenuProfile = " ";
                obj.LastDateModified = DateTime.Now;
                //obj.DateToBeBlocked = DateTime.Now;
                db.Staff.Add(obj);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.Message = "Mã nhân viên đã được sử dụng";
                return RedirectToAction("Index");
            }
        
        }


    }
}