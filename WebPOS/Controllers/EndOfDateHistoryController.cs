﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebPOS.Option;

namespace WebPOS.Controllers
{
    
    public class EndOfDateHistoryController : Controller
    {
        private CentralDbContext db = new CentralDbContext();
        [Authorize]
        public IActionResult Index()
        {
            IEnumerable<SelectListItem> items = db.Store.Select(c => new SelectListItem
            {
                Value = c.No,
                Text = c.No + " + " + c.Name

            }); ;
            ViewBag.StoreNo = items;
            ViewBag.From_Date = DateTime.Today.AddDays(-1).ToString("dd-MM-yyyy");
            ViewBag.To_Date = DateTime.Today.ToString("dd-MM-yyyy");
            return View();
        }
        [Authorize]
        public IActionResult ViewDetail()
        {
            return View();
        }
    }
}