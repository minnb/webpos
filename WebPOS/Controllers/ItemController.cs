﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebPOS.Dto;
using WebPOS.Models;
using WebPOS.Option;
using System.Data.SqlClient;
using System.Collections;
using System.Data;
using Microsoft.Extensions.Caching.Memory;
using WebPOS.Models.ViewModels;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WebPOS.Utils;
using WebPOS.Service;
using Microsoft.AspNetCore.Http;
using System.Xml;
using Formatting = Newtonsoft.Json.Formatting;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebPOS.Controllers
{
    
    public class ItemController : Controller
    {
        //private readonly ADO_NET _dbcon = new ADO_NET();
        private CentralDbContext db = new CentralDbContext();
       
        public ItemController()
        {
        }
        [Authorize]
        public IActionResult Index()
        {
            IEnumerable<SelectListItem> items = db.Store.Select(c => new SelectListItem
            {
                Value = c.No,
                Text = c.No + " + " + c.Name

            }); ;
            ViewBag.StoreNo = items;
            //ViewBag.store.Name = ViewBag.store.No + ViewBag.store.Name;
            return View();
        }
        [Authorize]
        public async Task<IActionResult> LoadData(string StoreNo, string ItemNo, string Barcode, string Description)
        {

            string BaseUrl = Startup.GetBaseUrl();
            string Token = Startup.GetTokenWebApi();
            string storeno = "";
            string itemno = "";
            string barcode = "";
            string description = "";
            
            if (StoreNo != null)
            {
                storeno = "&_storeNo=" + StoreNo;
            }
            
            if (ItemNo != null)
            {
                itemno = "&_itemNo=" + ItemNo;
            }
            if (Barcode != null)
            {
                barcode = "&_barcode=" + Barcode;
            }
            if (Description != null)
            {
                description = "&_description=" + Description;
            }
            string baseURL = BaseUrl + $"v1/WebApi/GetCheckSalesPrice?_token=" + Token + storeno + itemno + barcode + description;
            try
            {
                //Now we will have our using directives which would have a HttpClient 
                using (HttpClient client = new HttpClient())
                {
                    //Now get your response from the client from get request to baseurl.
                    //Use the await keyword since the get request is asynchronous, and want it run before next asychronous operation.
                    using (HttpResponseMessage res = await client.GetAsync(baseURL))
                    {
                        //Now we will retrieve content from our response, which would be HttpContent, retrieve from the response Content property.
                        using (HttpContent content = res.Content)
                        {
                            //Retrieve the data from the content of the response, have the await keyword since it is asynchronous.
                            var dataObj = await content.ReadAsStringAsync();
                            if (dataObj == "null")
                            {
                                return Json(new { draw = 0, recordsTotal = 0, recordsFiltered = 0, data = "" });
                            }
                            var list = JObject.Parse(dataObj)["ItemInfo"].Select(el => new { salesCode = (string)el["salesCode"], itemNo = (string)el["itemNo"], barcodeNo = (string)el["barcodeNo"], description = (string)el["description"], unitOfMeasureCode = (string)el["unitOfMeasureCode"], unitPrice = (string)el["unitPrice"], articleType = (string)el["articleType"], startingDate = (DateTime)el["startingDate"], endingDate = (DateTime)el["endingDate"], blocked = (string)el["blocked"], critical = (string)el["critical"] }).ToList();
                            //var data = JObject.Parse(dataObj);
                           
                            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                            // Skiping number of Rows count  
                            var start = Request.Form["start"].FirstOrDefault();
                            // Paging Length 10,20  
                            var length = Request.Form["length"].FirstOrDefault();
                            // Sort Column Name  
                            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                            // Sort Column Direction ( asc ,desc)  
                            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                            // Search Value from (Search box)  
                           // var searchValue = Request.Form["search[value]"].FirstOrDefault();

                            //Paging Size (10,20,50,100)  
                            int pageSize = length != null ? Convert.ToInt32(length) : 0;
                            int skip = start != null ? Convert.ToInt32(start) : 0;
                            int recordsTotal = 0;



                            //total number of rows count   
                            recordsTotal = list.Count();
                            //Paging   
                            var data2 = list.Skip(skip).Take(pageSize).ToList();
                            //Returning Json Data
                            //return View(customerData);

                            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data2 }); 
                            //return Json(data);

                        }
                        
                        
                    }
                }
            }
            catch 
            {
                throw;
              }

            

        }

        [Authorize]
        [HttpPost]
        public IActionResult PostListTransRawAsync(string StoreNo, string ItemNo, string Barcode, string Description)
        {
            return View();
        }
        [HttpGet]
        [Authorize]
        public IActionResult ViewDetail(string id)
        {

            ViewBag.sale = db.TransHeader.Find(id);
            IEnumerable<TransLine> stu = db.TransLine.Where(st => st.DocumentNo == id);

            //Tạo list
            //List<Student> stuList = stu.ToList();
            ViewBag.products = stu.ToList();
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "Sale");
                }

                return View("ViewDetail");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}