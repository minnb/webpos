﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebPOS.Dto;
using WebPOS.Models;
using WebPOS.Option;
using Microsoft.AspNetCore.Http;

namespace WebPOS.Controllers
{
    [Authorize]
    public class TenderTypeController : Controller
    {
        private CentralDbContext db = new CentralDbContext();

        public IActionResult Index()
        {
            ViewBag.stores = db.Store.FromSql("select l.* from Store l left join TenderType r on r.StoreNo = l.No where r.StoreNo is null").ToList();
         //ViewBag.stores = db.Store.ToList();
            return View();
        }

        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skiping number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction ( asc ,desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();

                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Customer data  
                var customerData = (from tempcustomer in db.TenderType
                                    select tempcustomer);

                //Sorting  
                // if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                // {
                // customerData = customerData.OrderBy(sortColumn + " " + sortColumnDirection);
                // }
               // Search  
                 if (!string.IsNullOrEmpty(searchValue))
                  {
                     customerData = customerData.Where(m => m.StoreNo == searchValue );
                 }

                //total number of rows count   
                recordsTotal = customerData.Count();
                //Paging   
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception)
            {
                throw;
            }

        }
        [HttpPost]
        public ActionResult AddTenderType(IFormCollection request)
        {
            string StoreNoNew = request["StoreNo"].ToString().Trim();
            var tendertypesetup = db.TenderTypeSetup.ToList();
            int total = tendertypesetup.Count();
            if(total > 0)
            {
                foreach (TenderTypeSetup row in tendertypesetup)
                {
                    TenderType tendertypenew = new TenderType
                    {
                        StoreNo = StoreNoNew,
                        Code = row.Code,
                        Description = row.Description,
                        ChangeTenderCode = " ",
                        BankAccountNo = " ",
                        BankAccountName = " ",
                        AccountNo = " ",
                        MayBeUsed = 1
                    };

                    db.TenderType.Add(tendertypenew);
                    db.SaveChanges();
                }
            }
                            
             
            
            return RedirectToAction("Index");
        }

      
    }
}