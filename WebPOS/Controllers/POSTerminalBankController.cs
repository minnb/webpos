﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebPOS.Dto;
using WebPOS.Option;

namespace WebPOS.Controllers
{
    [Authorize]
    public class POSTerminalBankController : Controller
    {
        private CentralDbContext db = new CentralDbContext();

        public IActionResult Index()
        {
            IEnumerable<SelectListItem> items = db.TenderTypeSetup.FromSql("select * from TenderTypeSetup where Code='ZCRE' or Code='ZDEB'").Select(c => new SelectListItem
            {
                Value = c.Code,
                Text = c.Code + " - " + c.Description

            }); ;
            ViewBag.TenderTypeCode = items;
            IEnumerable<SelectListItem> subcode = db.InformationSubcode.FromSql("select * from InformationSubcode where Code='BNKPOS'").Select(c => new SelectListItem
            {
                Value = c.Subcode,
                Text = c.Subcode + " - " + c.Description

            }); ;
            ViewBag.BankCode = subcode;
            IEnumerable<SelectListItem> store = db.Store.Select(c => new SelectListItem
            {
                Value = c.No,
                Text = c.No + " - " + c.Name

            }); ;
            ViewBag.StoreNo = store;
            IEnumerable<SelectListItem> posterminal = db.POSTerminal.Select(c => new SelectListItem
            {
                Value = c.No,
                Text = c.No

            }); ;
            ViewBag.POSTerminal = posterminal;
            return View();
        }

        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skiping number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction ( asc ,desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();

                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Customer data  
                var customerData = (from tempcustomer in db.POSTerminalBank
                                    select tempcustomer);

                //Sorting  
                // if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                // {
                // customerData = customerData.OrderBy(sortColumn + " " + sortColumnDirection);
                // }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    customerData = customerData.Where(m => m.StoreNo == searchValue || m.BankPOS == searchValue || m.POSTerminal == searchValue || m.TenderTypeCode == searchValue);
                }

                //total number of rows count   
                recordsTotal = customerData.Count();
                //Paging   
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception)
            {
                throw;
            }



        }
        [HttpGet]
        public JsonResult GetPOSTerminalList(string StoreNo)
        {
            var posterminal = new SelectList(db.POSTerminal.Where(c => c.StoreNo == StoreNo), "No", "No");
            return Json(posterminal);

        }
        [HttpPost]
        public ActionResult Add(IFormCollection request)
        {
            string lTenderTypeCode = request["TenderTypeCode"].ToString().Trim();
            string lBankPOS = request["BankPOS"].ToString().Trim();
            string lDescription = request["Description"].ToString().Trim();
            string lBankCode = request["BankCode"].ToString().Trim();
            string lStoreNo = request["StoreNo"].ToString().Trim();
            string lPOSTerminal = request["POSTerminal"].ToString().Trim();
            string lPOSNumber = request["POSNumber"].ToString().Trim();

            var SeachData = db.POSTerminalBank.Where(x => x.BankPOS == lBankPOS && x.POSTerminal == lPOSTerminal && x.TenderTypeCode == lTenderTypeCode).SingleOrDefault();
            if (SeachData == null)
            {
                POSTerminalBank banknew = new POSTerminalBank
                {
                    TenderTypeCode = lTenderTypeCode,
                    BankPOS = lBankPOS,
                    Description = lDescription,
                    BankCode = lBankCode,
                    StoreNo = lStoreNo,
                    POSTerminal = lPOSTerminal,
                    StoreNumber = "00" + lStoreNo,
                    POSNumber = lPOSNumber

                };

                db.POSTerminalBank.Add(banknew);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }


        }

        [HttpPost]
        public IActionResult Delete(string BankPOS)
        {
            string[] splitStr = BankPOS.Split(',');


            string BankPOS1 = splitStr[0];
            string TenderTypeCode = splitStr[1];
            string POSTerminal = splitStr[2];


            db.POSTerminalBank.Remove(db.POSTerminalBank.Where(x => x.BankPOS == BankPOS1 && x.POSTerminal == POSTerminal && x.TenderTypeCode == TenderTypeCode).SingleOrDefault());
            db.SaveChanges();
            return RedirectToAction("Index");

        }
        [HttpGet]
        public IActionResult Edit(string bankpos, string tendertypecode, string posterminal)
        {
            // ViewBag.stores = db.Store.ToList();
            //ViewBag.posterminal = db.POSTerminal.Where().ToList();
            try
            {
                ViewBag.posterminalbank = db.POSTerminalBank.Where(x => x.BankPOS == bankpos && x.POSTerminal == posterminal && x.TenderTypeCode == tendertypecode).FirstOrDefault();

                return View("Edit");
            }
            catch (Exception)
            {
                throw;
            }
        }
        [HttpPost]
        public IActionResult Edit(POSTerminalBank obj)
        {
            POSTerminalBank existing = db.POSTerminalBank.Where(x => x.BankPOS == obj.BankPOS && x.POSTerminal == obj.POSTerminal && x.TenderTypeCode == obj.TenderTypeCode).FirstOrDefault();
            existing.Description = obj.Description;
            existing.BankCode = obj.BankCode;
            existing.POSNumber = obj.POSNumber;
            db.Entry(existing).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
       
        private IHostingEnvironment _hostingEnvironment;

        public POSTerminalBankController(IHostingEnvironment environment)
        {
            _hostingEnvironment = environment;
        }
        [HttpPost]
        public async Task<IActionResult> ImportCSV(IFormFile postedFile)
        {
            string filePath = string.Empty;
            if (postedFile != null)
            {
                var path = Path.Combine(_hostingEnvironment.WebRootPath);
               
                
                filePath = path + @"\UploadCSV\" + Path.GetFileName(postedFile.FileName);
                string extension = Path.GetExtension(postedFile.FileName);
                if (extension == ".txt" || extension == ".csv")
                {
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await postedFile.CopyToAsync(stream);
                    }
                    string csvData = System.IO.File.ReadAllText(filePath);
                    using (var streamReader = System.IO.File.OpenText(filePath))
                    {
                        //var dbContext = new SampleDbContext();
                        while (!streamReader.EndOfStream)
                        {

                            var line = streamReader.ReadLine();
                            var data = line.Split(new[] { ',' });
                            var person = new POSTerminalBank() { TenderTypeCode = data[0], BankPOS = data[1], Description = data[2], BankCode = data[3], StoreNo = data[4], POSTerminal = data[5], StoreNumber = data[6], POSNumber = data[7] };
                            var SeachData = db.POSTerminalBank.Where(x => x.BankPOS == person.BankPOS && x.POSTerminal == person.POSTerminal && x.TenderTypeCode == person.TenderTypeCode).SingleOrDefault();
                            if (SeachData == null)
                            {
                                db.POSTerminalBank.Add(person);
                            }

                        }


                        db.SaveChanges();
                    }
                }
                
                else
                {
                    
                }
                
            }

            return RedirectToAction("Index");
        }
    }
}