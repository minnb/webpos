﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebPOS.Dto;
using WebPOS.Option;

namespace WebPOS.Controllers
{
    public class POSTerminalController : Controller
    {
        private CentralDbContext db = new CentralDbContext();
        //private CentralDbContext _context = new CentralDbContext();
        //public POSTerminalController(CentralDbContext context)
        //{
        //_context = context;
        //}
        // GET: /<controller>/  
        [Authorize]
        public IActionResult Index()
        {
            IEnumerable<SelectListItem> items = db.Store.Select(c => new SelectListItem
            {
                Value = c.No,
                Text = c.No + " + " + c.Name

            }); ;
            ViewBag.StoreNo = items;
            return View();
        }
        [Authorize]
        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skiping number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction ( asc ,desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();

                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Customer data  
                var customerData = (from tempcustomer in db.POSTerminal
                                    select tempcustomer);

                //Sorting  
               // if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
               // {
                  // customerData = customerData.OrderBy(sortColumn + " " + sortColumnDirection);
               // }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    customerData = customerData.Where(m => m.No == searchValue || m.StoreNo == searchValue || m.TerminalNetworkID == searchValue);
                }

                //total number of rows count   
                recordsTotal = customerData.Count();
                //Paging   
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception)
            {
                throw;
            }

        }
        [Authorize]
        [HttpGet]
        public IActionResult Edit(string id)
        {
           // ViewBag.stores = db.Store.ToList();
            ViewBag.posterminal = db.POSTerminal.Find(id);
            
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "POSTerminal");
                }

                return View("Edit");
            }
            catch (Exception)
            {
                throw;
            }
        }
        [HttpPost]
        public IActionResult Edit(POSTerminal obj)
        {
            POSTerminal existing = db.POSTerminal.Find(obj.No);
            existing.TerminalNetworkID = obj.TerminalNetworkID;
            existing.Description = obj.Description;
            if (obj.StyleProfile == null)
            {
                existing.StyleProfile = "";
            }
            else { existing.StyleProfile = obj.StyleProfile; }
            existing.LastDateModified = DateTime.Now;
            db.Entry(existing).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet]
        [Authorize]
        public IActionResult ViewDetail(string id)
        {
            //ViewBag.stores = db.Store.ToList();
            ViewBag.posterminal = db.POSTerminal.Find(id);
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "POSTerminal");
                }

                return View("ViewDetail");
            }
            catch (Exception)
            {
                throw;
            }
        }
       

        [HttpPost]
        [Authorize]
        public IActionResult Delete(string id)
        {
            db.POSTerminal.Remove(db.POSTerminal.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
            
        }
        
      
        [HttpGet]
        [Authorize]
        public IActionResult Add()
        {
            IEnumerable<SelectListItem> items = db.Store.Select(c => new SelectListItem
            {
                Value = c.No,
                Text = c.No + " + " + c.Name

            }); ;
            ViewBag.StoreNo = items;
            return View();
        }
        public JsonResult CheckUsernameAvailability(string userdata)
        {
            System.Threading.Thread.Sleep(200);
           if (userdata.Length == 1)
            {
                userdata = "0" + userdata;
            }
           
                var SeachData = db.POSTerminal.Where(x => x.No == (x.StoreNo + userdata)).SingleOrDefault();
                if (SeachData != null)
                {
                    return Json(1);
                }

                else
                {
                    return Json(0);
                }
            
           

        }
        
        [HttpPost]
        public IActionResult Add(POSTerminal obj)
        {
            
            if (ModelState.IsValid)
            {
                var SeachData = db.POSTerminal.Where(x => x.No == (obj.StoreNo + obj.No)).SingleOrDefault();
                if (SeachData == null)
                {
                    obj.No = obj.StoreNo + obj.No;

                    obj.BillNoseri = "0";
                    obj.CustomerDisplayText1 = "0";
                    obj.CustomerDisplayText2 = "0";
                    obj.DefaultPriceGroup = "0";
                    obj.DefaultSalesType = "0";
                    obj.DualDisHost = "0";
                    obj.FunctionalityProfile = "0";
                    obj.HardwareProfile = "0";
                    obj.InterfaceProfile = "0";
                    obj.MACAddress = "0";
                    obj.MenuProfile = "0";
                    obj.Placement = "0";
                    obj.PrintReceiptLogo = "0";
                    obj.SalesTypeFilter = "0";
                    if (obj.StyleProfile == null)
                    {
                        obj.StyleProfile = "";
                    }
                    else { obj.StyleProfile = obj.StyleProfile; }
                    //obj.StyleProfile = "0";
                    obj.LastDateModified = DateTime.Now;

                    obj.TerminalIPAddress = "0";
                    db.POSTerminal.Add(obj);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                { ViewBag.Message = "Mã POS đã được sử dụng";
                    return RedirectToAction("Index");
                }
               
            }
            return View(obj);
        }


    }
}
