﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebPOS.Dto;
using WebPOS.Models;
using WebPOS.Option;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebPOS.Controllers
{

    [Authorize]
    public class StorePriceGroupController : Controller
    {
        private CentralDbContext db = new CentralDbContext();
        //private CentralDbContext _context = new CentralDbContext();
        //public POSTerminalController(CentralDbContext context)
        //{
        //_context = context;
        //}
        // GET: /<controller>/  
        public IActionResult Index()
        {
            IEnumerable<SelectListItem> items = db.Store.FromSql("select l.* from Store l left join StorePriceGroup r on r.Store = l.No where r.Store is null").Select(c => new SelectListItem
            {
                Value = c.No,
                Text = c.No + " + " + c.Name

            }); ;
            ViewBag.StoreNo = items;
            //ViewBag.stores = db.Store.FromSql("select l.* from Store l left join StorePriceGroup r on r.Store = l.No where r.Store is null	").ToList();
            //ViewBag.stores = db.Store.ToList();
            return View();
        }

        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skiping number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction ( asc ,desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();

                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Customer data  
                var customerData = (from tempcustomer in db.StorePriceGroup
                                    select tempcustomer);

                //Sorting  
                // if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                // {
                // customerData = customerData.OrderBy(sortColumn + " " + sortColumnDirection);
                // }
                //Search  
               // if (!string.IsNullOrEmpty(searchValue))
              //  {
              //      customerData = customerData.Where(m => m.No == searchValue || m.StoreNo == searchValue || m.TerminalNetworkID == searchValue);
              //  }

                //total number of rows count   
                recordsTotal = customerData.Count();
                //Paging   
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception)
            {
                throw;
            }

        }
        
        
        [HttpPost]
        public ActionResult AddStorePriceGroup(IFormCollection request)
        {
            StorePriceGroup storenew1 = new StorePriceGroup
            {
                Store = request["StoreNo"].ToString().Trim(),
                PriceGroupCode = request["StoreNo"].ToString().Trim(),
                Priority = 1,
                Type = 1,
                ReplicationCounter = 0
            };
            StorePriceGroup storenew2 = new StorePriceGroup
            {
                Store = request["StoreNo"].ToString().Trim(),
                PriceGroupCode = "ALL",
                Priority = 10,
                Type = 1,
                ReplicationCounter = 0
            };
            db.StorePriceGroup.Add(storenew1);
            db.StorePriceGroup.Add(storenew2);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


    }
}