﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebPOS.Dto;
using WebPOS.Models;
using WebPOS.Option;
using System.Data.SqlClient;
using System.Collections;
using System.Data;
using Microsoft.Extensions.Caching.Memory;
using WebPOS.Models.ViewModels;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WebPOS.Utils;
using WebPOS.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebPOS.Controllers
{
    [Authorize]
    public class ReportstaffController : Controller
    {
        private readonly ADO_NET _dbcon = new ADO_NET();
        private CentralDbContext db = new CentralDbContext();
        public ReportstaffController()
        {
        }
        [HttpGet]
        public IActionResult Index(string StoreNo, string CashierID, string From_Date, string To_Date)
        {
            IEnumerable<SelectListItem> items = db.Store.Select(c => new SelectListItem
            {
                Value = c.No,
                Text = c.No + " + " + c.Name,
                Selected = (c.No == StoreNo) ? true : false

            }); ;
            ViewBag.StoreNo = items;
            ViewBag.From_Date = DateTime.Today.AddDays(-1).ToString("dd-MM-yyyy");
            ViewBag.To_Date = DateTime.Today.ToString("dd-MM-yyyy");
            DataSet dataSet = new DataSet("TransRaw");
            Hashtable param = new Hashtable();

           

            try
            {
                if (string.IsNullOrEmpty(StoreNo) && string.IsNullOrEmpty(CashierID) && string.IsNullOrEmpty(From_Date) && string.IsNullOrEmpty(To_Date))
                {
                    param.Add("@StoreNo", "");
                    param.Add("@CashierID", "");
                    param.Add("@From_Date", "");
                    param.Add("@To_Date", "");
                    var connectStr = Constants.DBConnectStringCentral();
                    dataSet = _dbcon.ExecuteProcedure(connectStr, "SP_WEB_CHECK_TRANS_CASHIER", param);
                }
                else if (string.IsNullOrEmpty(StoreNo) && string.IsNullOrEmpty(CashierID))
                {
                    param.Add("@StoreNo", "");
                    param.Add("@CashierID", "");
                    param.Add("@From_Date", ConvertDateTime.Date2Char(From_Date));
                    param.Add("@To_Date", ConvertDateTime.Date2Char(To_Date));
                    dataSet = _dbcon.ExecuteProcedure(Constants.DBConnectStringCentral(), "SP_WEB_CHECK_TRANS_CASHIER", param);
                    ViewBag.To_Date = To_Date;
                    ViewBag.From_Date = From_Date;
                }
                else if (string.IsNullOrEmpty(StoreNo) && string.IsNullOrEmpty(CashierID) && string.IsNullOrEmpty(To_Date))
                {
                    param.Add("@CouponCode", "");
                    param.Add("@CashierID", "");
                    param.Add("@From_Date", ConvertDateTime.Date2Char(From_Date));
                    param.Add("@To_Date", "");
                    dataSet = _dbcon.ExecuteProcedure(Constants.DBConnectStringCentral(), "SP_WEB_CHECK_TRANS_CASHIER", param);

                   ViewBag.From_Date = From_Date;
                }
                else if (string.IsNullOrEmpty(StoreNo) && string.IsNullOrEmpty(CashierID) && string.IsNullOrEmpty(From_Date))
                {
                    param.Add("@StoreNo", "");
                    param.Add("@CashierID", "");
                    param.Add("@To_Date", ConvertDateTime.Date2Char(To_Date));
                    param.Add("@From_Date", "");
                    dataSet = _dbcon.ExecuteProcedure(Constants.DBConnectStringCentral(), "SP_WEB_CHECK_TRANS_CASHIER", param);

                   ViewBag.To_Date = To_Date;
                }
                else if (string.IsNullOrEmpty(StoreNo))
                {
                    param.Add("@StoreNo", "");
                    param.Add("@ItemName", CashierID);
                    param.Add("@From_Date", ConvertDateTime.Date2Char(From_Date));
                    param.Add("@To_Date", ConvertDateTime.Date2Char(To_Date));
                    dataSet = _dbcon.ExecuteProcedure(Constants.DBConnectStringCentral(), "SP_WEB_CHECK_TRANS_CASHIER", param);
                    ViewBag.From_Date = From_Date;
                    ViewBag.To_Date = To_Date;
                   ViewBag.CashierID = CashierID;
                }
                else if (string.IsNullOrEmpty(CashierID))
                {
                    param.Add("@StoreNo", StoreNo);
                    param.Add("@CashierID", "");
                    param.Add("@From_Date", ConvertDateTime.Date2Char(From_Date));
                    param.Add("@To_Date", ConvertDateTime.Date2Char(To_Date));
                    dataSet = _dbcon.ExecuteProcedure(Constants.DBConnectStringCentral(), "SP_WEB_CHECK_TRANS_CASHIER", param);
                    ViewBag.From_Date = From_Date;
                    ViewBag.To_Date = To_Date;
                    //ViewBag.StoreNo = StoreNo;
                }
                else if (string.IsNullOrEmpty(From_Date))
                {
                    param.Add("@StoreNo", StoreNo);
                    param.Add("@CashierID", CashierID);
                    param.Add("@From_Date", "");
                    param.Add("@To_Date", ConvertDateTime.Date2Char(To_Date));
                    dataSet = _dbcon.ExecuteProcedure(Constants.DBConnectStringCentral(), "SP_WEB_CHECK_TRANS_CASHIER", param);
                    ViewBag.To_Date = To_Date;
                    ViewBag.CashierID = CashierID;
                   // ViewBag.StoreNo = StoreNo;
                }
                else if (string.IsNullOrEmpty(To_Date))
                {
                    param.Add("@StoreNo", StoreNo);
                    param.Add("@CashierID", CashierID);
                    param.Add("@EndingDate", "");
                    param.Add("@From_Date", ConvertDateTime.Date2Char(From_Date));
                    dataSet = _dbcon.ExecuteProcedure(Constants.DBConnectStringCentral(), "SP_WEB_CHECK_TRANS_CASHIER", param);
                    ViewBag.From_Date = From_Date;
                    ViewBag.CashierID = CashierID;
                    //ViewBag.StoreNo = StoreNo;
                }
                else
                {
                    param.Add("@StoreNo", StoreNo);
                    param.Add("@CashierID", CashierID);
                    param.Add("@From_Date", ConvertDateTime.Date2Char(From_Date));
                    param.Add("@To_Date", ConvertDateTime.Date2Char(To_Date));
                    dataSet = _dbcon.ExecuteProcedure(Constants.DBConnectStringCentral(), "SP_WEB_CHECK_TRANS_CASHIER", param);
                  
                }
            }
            catch
            {

            }

            return View(dataSet);


        }


        [HttpPost]
        public IActionResult PostListTransRawAsync(string CouponCode, string ItemName, string StartingDate, string EndingDate)
        {
            return View();
        }
       
    }
}