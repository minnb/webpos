﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebPOS.Option;
using WebPOS.Utils;
using static System.Net.Mime.MediaTypeNames;

namespace WebPOS.Controllers
{
    
    public class SalePriceController : Controller
    {
        private CentralDbContext db = new CentralDbContext();
        [Authorize]
        public IActionResult Index()
        {
            string lstorecode = "1511";
            IEnumerable<SelectListItem> items = db.Store.Select(c => new SelectListItem
            {
                Value = c.No,
                Text = c.No + " + " + c.Name,
                Selected = (c.No == lstorecode) ? true : false
            }); ;
            ViewBag.StoreNo = items;
            string ldate1 = DateTime.Today.ToString("dd-MM-yyyy");
            ViewBag.date = ldate1;
            return View();
        }
        [Authorize]
        public IActionResult CheckPriceScanGo(string date, string storeCode, string barcode, string quantity)
        {
            string RestApiUrl = Startup.GetRestApiUrl();
            string Token = Startup.GetTokenRestApi();
            //DateTime today = date;
            //string ldate = ConvertDateTime.Date2Char(today);
            
            string ldate = DateTime.Today.ToString("yyyyMMdd");
            string lstorecode = "";
            string lbarcode = "";
            string lquantity = "";
            ViewBag.storeCode = lstorecode;
           // ViewBag.barcode = lbarcode;
           // ViewBag.quantity = lquantity;
            //View
            if (date != null)
            {
                ldate = ConvertDateTime.Date2Char(date);
                ViewBag.date = date;
            }
            if (storeCode != null)
            {
                lstorecode = storeCode;
            }
            if (barcode != null)
            {
                lbarcode = barcode;
                ViewBag.barcode = barcode;
            }
            if (quantity != null)
            {
                lquantity = quantity;
                ViewBag.quantity = quantity;
            }
           


            string url = RestApiUrl + "v1/Api/GetSkuPriceRequest";
            string data = "{ \"date\": \""+ldate+ "\",  \"storeCode\": \""+lstorecode+ "\",  \"barcode\": \""+lbarcode+ "\",  \"quantity\": "+lquantity+"}";

            WebRequest myReq = WebRequest.Create(url);
            myReq.Method = "POST";
            

            UTF8Encoding enc = new UTF8Encoding();
            myReq.Headers.Add("accept: application/json");
            myReq.Headers.Add("_token:"+ Token +"");

            myReq.Headers.Add("Content-Type: application/json");
            using (Stream ds = myReq.GetRequestStream())
            {
                ds.Write(enc.GetBytes(data), 0, data.Length);
            }
            WebResponse wr = myReq.GetResponse();
            Stream receiveStream = wr.GetResponseStream();
            StreamReader reader = new StreamReader(receiveStream, Encoding.UTF8);
            string content = reader.ReadToEnd();
           

            return Json(new { success = true, responseText = content });
        }
        [Authorize]
        public async Task<IActionResult> CheckOrderScanGo(string barcode2)
        {
            string WebApiUrl = Startup.GetBaseUrl();
            string WebApiUrl_Token = Startup.GetTokenWebApi();
            string barcode = "";
            //string type = "";
            if (barcode2 != null)
            {
                barcode = "&barcode=" + barcode2;
            }

            
            string baseURL = WebApiUrl + $"v1/ScanGo/GetOrder?token=" + WebApiUrl_Token + barcode;
            try
            {
                //Now we will have our using directives which would have a HttpClient 
                using (HttpClient client = new HttpClient())
                {
                    //Now get your response from the client from get request to baseurl.
                    //Use the await keyword since the get request is asynchronous, and want it run before next asychronous operation.
                    using (HttpResponseMessage res = await client.GetAsync(baseURL))
                    {
                        //Now we will retrieve content from our response, which would be HttpContent, retrieve from the response Content property.
                        using (HttpContent content = res.Content)
                        {
                            string dataObj = await content.ReadAsStringAsync();

                           
                            return Json(new { success = true, responseText = dataObj });
                           


                        }
                    }
                }
            }
            catch
            {
                throw;
            }


        }
    }
}