﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebPOS.Dto;
using WebPOS.Option;

namespace WebPOS.Controllers
{
    [Authorize]
    public class UserroleController : Controller
    {
        private CentralDbContext db = new CentralDbContext();
        public IActionResult Index()
        {
           // var test = db.User.FromSql("select u.* from [User] u left join UserRole r on r.UserID = u.UserName where r.UserID is null").ToList();
            //var customerData = (from tempcustomer in db.User
                               // select tempcustomer);
            IEnumerable<SelectListItem> itemss = db.Role.Select(c => new SelectListItem
            {
                Value = c.RoleID,
                Text = c.RoleName

            }); ;
            ViewBag.Roles = itemss;
            IEnumerable<SelectListItem> items = db.User.FromSql("select u.* from [User] u left join UserRole r on r.UserID = u.UserName where r.UserID is null").Select(c => new SelectListItem
            {
                Value = c.UserName,
                Text = c.UserName

            }); ;
            ViewBag.Users = items;
            IEnumerable<SelectListItem> itemsss = db.Staff.FromSql("select u.* from Staff u left join UserRole r on r.UserID = u.ID where r.UserID is null").Select(c => new SelectListItem
            {
                Value = c.ID,
                Text = c.ID

            }); ;
            ViewBag.Staffs = itemsss;
            return View();
        }
        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skiping number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction ( asc ,desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();

                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Customer data  
                var customerData = (from tempcustomer in db.UserRole
                                    select tempcustomer);

                //Sorting  
                // if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                // {
                // customerData = customerData.OrderBy(sortColumn + " " + sortColumnDirection);
                // }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    customerData = customerData.Where(m => m.UserID == searchValue || m.RoleID == searchValue);
                }

                //total number of rows count   
                recordsTotal = customerData.Count();
                //Paging   
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception)
            {
                throw;
            }

        }
        [HttpPost]
        public IActionResult AddUserRole(IFormCollection obj)
        {

            string values = Request.Form["duallistbox_demo1[]"];
            string roleid = Request.Form["RoleID"];
            //string str = "Valve, Body, Handle, Cover, None";

            //Dictionary<int, string> ddlBind = new Dictionary<int, string>();

            string[] splitStr = values.Split(',');

            for (int i = 0; i < splitStr.Length; i++)
            {
                UserRole userole = new UserRole
                {
                    UserID = splitStr[i],
                    RoleID = roleid,
                    Status = "1"
                };

                db.UserRole.Add(userole);
                db.SaveChanges();
            }


            return RedirectToAction("Index");
        }
        [HttpPost]
        public IActionResult AddStaffRole(IFormCollection obj)
        {

            string values = Request.Form["duallistbox_demo2[]"];
            string roleid = Request.Form["RoleID"];
            //string str = "Valve, Body, Handle, Cover, None";

            //Dictionary<int, string> ddlBind = new Dictionary<int, string>();

            string[] splitStr = values.Split(',');

            for (int i = 0; i < splitStr.Length; i++)
            {
                UserRole userole = new UserRole
                {
                    UserID = splitStr[i],
                    RoleID = roleid,
                    Status = "1"
                };

                db.UserRole.Add(userole);
                db.SaveChanges();
            }


            return RedirectToAction("Index");
        }
    }
}