﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebPOS.Dto;
using WebPOS.Models;
using WebPOS.Option;

namespace WebPOS.Controllers
{
    [Authorize]
    public class RoleController : Controller
    {
        private CentralDbContext db = new CentralDbContext();
         
        public IActionResult Index()
        {
            
            return View();
        }

        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skiping number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction ( asc ,desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();

                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Customer data  
                var customerData = (from tempcustomer in db.Role
                                    select tempcustomer);

                //Sorting  
                // if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                // {
                // customerData = customerData.OrderBy(sortColumn + " " + sortColumnDirection);
                // }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    customerData = customerData.Where(m =>m.RoleName == searchValue);
                }

                //total number of rows count   
                recordsTotal = customerData.Count();
                //Paging   
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception)
            {
                throw;
            }

        }

        [HttpGet]
        public IActionResult Edit(string id)
        {
           // string newid = @"VINGROUP\";
            //string newidnew = newid + id;
            ViewBag.userrole = db.Role.Find(id);
            DataSet ds = new DataSet();
            List<string> menus_id = db.LinkRolesMenus.Where(s => s.RoleID == id).Select(s => s.FunctionID.ToString()).ToList();
            ds = ToDataSet(db.Function.ToList());
            DataTable table = ds.Tables[0];
            DataRow[] parentMenus = table.Select("ParentId = 0");

            var sb = new StringBuilder();
            string unorderedList = GenerateUL(parentMenus, table, sb, menus_id);
            ViewBag.menu = unorderedList;
            return View();
        }
        [HttpPost]
        public IActionResult Edit(Role obj)
        {
            Role existing = db.Role.Find(obj.RoleID);
            existing.RoleName = obj.RoleName;
            existing.Description = obj.Description;
            existing.CreatedDate = DateTime.Now;
            // existing.LastDateModified = DateTime.Now;
            db.Entry(existing).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

       

        [HttpPost]
        public IActionResult Delete(string id)
        {
           // string newid = @"VINGROUP\";
            //string newidnew = newid + id;
            db.Role.Remove(db.Role.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");

        }

        [HttpGet]
        public IActionResult Add()
        {
            // ViewBag.stores = db.Store.ToList();
            return View();
        }
        public JsonResult CheckUsernameAvailability(string userdata)
        {
            System.Threading.Thread.Sleep(200);
            //string newid = @"VINGROUP\";
            //string newidnew = newid + userdata;
            var SeachData = db.Role.Where(x => x.RoleID == userdata).SingleOrDefault();
            if (SeachData != null)
            {
                return Json(1);
            }
            else
            {
                return Json(0);
            }

        }
        [HttpPost]
        public IActionResult Add(Role obj)
        {
            //  POSTerminal existing = db.POSTerminal.Find(obj.No);
            //obj.WindowsSecurityID = " ";
            obj.CreatedDate = DateTime.Now;
            //obj.UserName = @"VINGROUP\" + obj.UserName;

            db.Role.Add(obj);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        
        public IActionResult Update(string id, List<int> roles)
        {
            var temp = db.LinkRolesMenus.Where(s => s.RoleID == id);
            foreach (var item in temp)
            {
                db.LinkRolesMenus.Remove(item);
            }

            foreach (var role in roles)
            {
                db.LinkRolesMenus.Add(new LinkRolesMenus { FunctionID = role, RoleID = id });
            }

            db.SaveChanges();

            return Json(new { status = true, message = "Successfully Updated Role!" });
        }
        private string GenerateUL(DataRow[] menu, DataTable table, StringBuilder sb, List<string> menus_id)
        {
            if (menu.Length > 0)
            {
                foreach (DataRow dr in menu)
                {
                    string id = dr["Id"].ToString();
                    string handler = dr["Url"].ToString();
                    string menuText = dr["Name"].ToString();
                    string icon = dr["CssClass"].ToString();

                    string pid = dr["Id"].ToString();
                    string parentId = dr["ParentId"].ToString();

                    string status = (menus_id.Contains(id)) ? "Checked" : "";

                    DataRow[] subMenu = table.Select(String.Format("ParentId = '{0}'", pid));
                    if (subMenu.Length > 0 && !pid.Equals(parentId))
                    {
                        string line = String.Format(@"<li class=""tree-branch""has""><input type=""checkbox"" name=""subdomain[]"" id=""{5}"" value=""{1}"" {4}><label> {1}</label>", handler, menuText, icon, "target", status, id);
                        sb.Append(line);

                        var subMenuBuilder = new StringBuilder();
                        sb.AppendLine(String.Format(@"<ul class=""tree-branch-children"">"));
                        sb.Append(GenerateUL(subMenu, table, subMenuBuilder, menus_id));
                        sb.Append("</ul>");
                    }
                    else
                    {
                        string line = String.Format(@"<li class=""tree-branch""><input type=""checkbox"" name=""subdomain[]"" id=""{5}"" value=""{1}"" {4}><label>{1}</label>", handler, menuText, icon, "target", status, id);
                        sb.Append(line);
                    }
                    sb.Append("</li>");
                }
            }
            return sb.ToString();
        }

        public DataSet ToDataSet<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dataTable);
            return ds;
        }
    }
}