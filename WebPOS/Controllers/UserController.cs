﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using WebPOS.Dto;
using WebPOS.Models;
using WebPOS.Option;

namespace WebPOS.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private CentralDbContext db = new CentralDbContext();
        static readonly HttpClient client = new HttpClient();
        private readonly IConfiguration _configuration;
        private readonly string LDAP_PATH;
        private readonly string LDAP_DOMAIN;
        public UserController(IConfiguration configuration)
        {
            _configuration = configuration;
            LDAP_PATH = _configuration["LDAP:LDAP_PATH"];
            LDAP_DOMAIN = _configuration["LDAP:LDAP_DOMAIN"];
        }

        public IActionResult Index()
        {
            IEnumerable<SelectListItem> items = db.Store.Select(c => new SelectListItem
            {
                Value = c.No,
                Text = c.No + " + " + c.Name

            }); ;
            ViewBag.StoreNo = items;
            return View();
        }

        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skiping number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction ( asc ,desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();

                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Customer data  
                var customerData = (from tempcustomer in db.User
                                    select tempcustomer);

                //Sorting  
                // if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                // {
                // customerData = customerData.OrderBy(sortColumn + " " + sortColumnDirection);
                // }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    customerData = customerData.Where(m => m.UserName == searchValue || m.FullName == searchValue);
                }

                //total number of rows count   
                recordsTotal = customerData.Count();
                //Paging   
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception)
            {
                throw;
            }

        }
    
        [HttpGet]
        public IActionResult Edit(string id)
        {
            string newid = @"VINGROUP\";
            string newidnew = newid + id;
            ViewBag.user = db.User.Find(newidnew);
            IEnumerable<SelectListItem> items = db.Store.Select(c => new SelectListItem
            {
                Value = c.No,
                Text = c.No + " + " + c.Name

            }); ;
            ViewBag.StoreNo = items;
            ViewBag.role = db.Role.ToList();
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "User");
                }

                return View("Edit");
            }
            catch (Exception)
            {
                throw;
            }
        }
        [HttpPost]
        public IActionResult Edit(User obj)
        {
            User existing = db.User.Find(obj.UserName);
           
            existing.StoreNo = obj.StoreNo;
           // existing.LastDateModified = DateTime.Now;
            db.Entry(existing).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpPost]
        public IActionResult AddRoleUser(IFormCollection request)
        {
            string userid = request["UserName"].ToString().Trim();
            //UserRole rolenew = new UserRole
            // {
            string roleid = request["RoleID"].ToString().Trim();
              
            var temp = db.UserRole.Where(s => s.UserID == userid);
            foreach (var item in temp)
            {
                db.UserRole.Remove(item);
            }

            UserRole rolenew = new UserRole
            {
                UserID = userid,
                RoleID = roleid,
                Status = "1"

            };
            db.UserRole.Add(rolenew);
            db.SaveChanges();

            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult ViewDetail(string id)
        {
            string newid = @"VINGROUP\";
            string newidnew = newid + id;
            ViewBag.posterminal = db.User.Find(newidnew);
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "User");
                }

                return View("ViewDetail");
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public IActionResult Delete(string id)
        {
            string newid = @"VINGROUP\";
            string newidnew = newid + id;
            db.User.Remove(db.User.Find(newidnew));
            db.SaveChanges();
            return RedirectToAction("Index");

        }

        [HttpGet]
        public IActionResult Add()
        {
            IEnumerable<SelectListItem> items = db.Store.Select(c => new SelectListItem
            {
                Value = c.No,
                Text = c.No + " + " + c.Name

            }); ;
            ViewBag.StoreNo = items;
            return View();
        }
        public JsonResult CheckUsernameAvailability(string userdata)
        {
            System.Threading.Thread.Sleep(200);
            using (var ctx = new PrincipalContext(ContextType.Domain))
            {
                using (var user = UserPrincipal.FindByIdentity(ctx, userdata))
                {
                    string newid = @"VINGROUP\";
                    string newidnew = newid + userdata;
                    var SeachData = db.User.Where(x => x.UserName == newidnew).SingleOrDefault();
                    if (SeachData == null && user != null)
                    {
                        return Json(0);
                    }
                    else
                    {
                        return Json(1);
                    }
                }
            }
                    
            

        }
        [HttpPost]
        public IActionResult Add(User obj)
        {
            UserAD infoUser = new UserAD();
            infoUser = CheckUserAD(obj.UserName);
            obj.WindowsSecurityID = " ";
            //obj.AuthenticationEmail = " ";
            obj.UserName = @"VINGROUP\" + obj.UserName;
            obj.FullName = infoUser.Name;
            obj.AuthenticationEmail = infoUser.EmailAddress;
            //obj.ExpiryDate = DateTime.Now;
            db.User.Add(obj);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        private UserAD CheckUserAD(string _username)
        {
            UserAD infoAD = new UserAD();
            using (var ctx = new PrincipalContext(ContextType.Domain))
            {
                using (var user = UserPrincipal.FindByIdentity(ctx, _username))
                {
                    if (user != null)
                    {
                        infoAD.UserName = user.ToString();
                        infoAD.EmailAddress = user.EmailAddress;
                        infoAD.DisplayName = user.DisplayName;
                        infoAD.Surname = user.Surname;
                        infoAD.Name = user.Name;
                        infoAD.MiddleName = user.MiddleName;
                        infoAD.SamAccountName = user.SamAccountName;
                        infoAD.UserPrincipalName = user.UserPrincipalName;
                        infoAD.VoiceTelephoneNumber = user.VoiceTelephoneNumber;
                        infoAD.Sid = user.Sid.ToString();
                        infoAD.Guid = user.Guid.ToString();
                        user.Dispose();
                    }
                }
            }
            return infoAD;
        }

    }
}