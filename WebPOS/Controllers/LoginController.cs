﻿using System;
using System.Collections.Generic;
using System.Data;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using WebPOS.Dto;
using WebPOS.Models.ViewModels;
using WebPOS.Option;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebPOS.Controllers
{
    public class LoginController : Controller
    {
        static readonly HttpClient client = new HttpClient();
        private readonly IConfiguration _configuration;
        private readonly string LDAP_PATH;
        private readonly string LDAP_DOMAIN;
        public LoginController(IConfiguration configuration)
        {
            _configuration = configuration;
            LDAP_PATH = _configuration["LDAP:LDAP_PATH"];
            LDAP_DOMAIN = _configuration["LDAP:LDAP_DOMAIN"];
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Index(int? errorId)
        {
            if (errorId > 0)
            {
                ViewBag.Error = "Tài khoản hoặc mật khẩu không đúng !";
            }
            if (!String.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("dang-nhap")]
        public async Task<IActionResult> PostLogin(IFormCollection request)
        {
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            LoginViewModel _login = new LoginViewModel
            {
                UserName = request["username"].ToString().Trim(),
                Password = request["password"].ToString().Trim()
            };

            //User Entities = new User();
            var context1 = new CentralDbContext();
           
            //string UserNameN = _login.UserName;
            var result = context1.User.Where(l => l.UserName == @"VINGROUP\" + _login.UserName).FirstOrDefault<User>();
            var staff = context1.Staff.Where(l => l.ID == _login.UserName).FirstOrDefault();
            // var role
            
            // var userrole = context1.Role.Where(e => e.RoleID == role.RoleID).FirstOrDefault();
            ClaimsIdentity identity = null;
           // bool isAuthenticated = false;
            // var result = query.ToList();
            if (result != null)
            {
                var role = context1.UserRole.Where(m => m.UserID == result.UserName).FirstOrDefault();
                try
                {
                    using (var context = new PrincipalContext(ContextType.Domain, LDAP_DOMAIN, "service_acct_user", "service_acct_pswd"))
                   {
                        if (context.ValidateCredentials(_login.UserName, _login.Password))
                       {
                           using (var de = new DirectoryEntry(LDAP_PATH))
                           using (var ds = new DirectorySearcher(de))
                            {
                                if (role != null)
                                {
                                    List<Function> menus = (from f in context1.Function
                                                            join l in context1.LinkRolesMenus on f.Id equals l.FunctionID

                                                            where l.RoleID == role.RoleID
                                                            select new Function() { Id = f.Id, Name = f.Name, ParentId = f.ParentId, CssClass = f.CssClass, IsActive = f.IsActive, SortOrder = f.SortOrder, Url = f.Url }).ToList();
                                    DataSet ds1 = new DataSet();
                                    ds1 = ToDataSet(menus);
                                    DataTable table = ds1.Tables[0];
                                    DataRow[] parentMenus = table.Select("ParentId = 0");

                                    var sb = new StringBuilder();
                                    string menuString = GenerateUL(parentMenus, table, sb);
                                    identity = new ClaimsIdentity(new[] {
                                    new Claim(ClaimTypes.Name, result.FullName),
                                    new Claim(ClaimTypes.Role, role.RoleID),
                                    new Claim(ClaimTypes.UserData, menuString)
                                }, CookieAuthenticationDefaults.AuthenticationScheme);

                                   
                                        var principal = new ClaimsPrincipal(identity);
                                       // var login = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                                   
                                    return RedirectToAction("Index", "Home"); ;
                                  
                                }
                                else
                                {
                                    identity = new ClaimsIdentity(new[] {
                                    new Claim(ClaimTypes.Name, _login.UserName)
                                  
                                }, CookieAuthenticationDefaults.AuthenticationScheme);

                                   // isAuthenticated = true;
                                   // if (isAuthenticated)
                                  //  {
                                        var principal = new ClaimsPrincipal(identity);
                                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);


                                    return RedirectToAction("Index", "Home"); ;
                                   // }
                                   // return View();
                                }
                               
                            }
                       }
                       else
                       {
                            return RedirectToAction("Index", "Login", new { @errorId = 1 });
                        }
                   }

                }
                catch (Exception ex)
                {
                    throw ex;
                    //return RedirectToAction("Index", "Login", new { @errorId = 1 });
                }
            }
            if ((result == null) && (staff != null))
            {
                try
                {
                   
                        if (staff.ID == _login.UserName && staff.Password == _login.Password)
                        {
                           
                            var role2 = context1.UserRole.Where(m => m.UserID == staff.ID).FirstOrDefault();
                        if (role2 != null)
                        {
                            List<Function> menus = (from f in context1.Function
                                                    join l in context1.LinkRolesMenus on f.Id equals l.FunctionID

                                                    where l.RoleID == role2.RoleID
                                                    select new Function() { Id = f.Id, Name = f.Name, ParentId = f.ParentId, CssClass = f.CssClass, IsActive = f.IsActive, SortOrder = f.SortOrder, Url = f.Url }).ToList();
                            DataSet ds1 = new DataSet();
                            ds1 = ToDataSet(menus);
                            DataTable table = ds1.Tables[0];
                            DataRow[] parentMenus = table.Select("ParentId = 0");

                            var sb = new StringBuilder();
                            string menuString = GenerateUL(parentMenus, table, sb);
                            identity = new ClaimsIdentity(new[] {
                                    new Claim(ClaimTypes.Name, staff.FirstName),
                                    new Claim(ClaimTypes.Role, role2.RoleID),
                                    new Claim(ClaimTypes.GroupSid , staff.StoreNo),
                                    new Claim(ClaimTypes.UserData, menuString)
                                }, CookieAuthenticationDefaults.AuthenticationScheme);

                           // isAuthenticated = true;
                            //if (isAuthenticated)
                           // {
                                var principal = new ClaimsPrincipal(identity);
                            //  var login = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);


                            return RedirectToAction("Index", "Home"); ;
                            //}
                           // return View();
                        }
                        else
                        {
                            identity = new ClaimsIdentity(new[] {
                                    new Claim(ClaimTypes.Name, _login.UserName)
                                   
                                }, CookieAuthenticationDefaults.AuthenticationScheme);

                           // isAuthenticated = true;
                           // if (isAuthenticated)
                           // {
                                var principal = new ClaimsPrincipal(identity);
                            // var login = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);


                            return RedirectToAction("Index", "Home"); ;
                          //  }
                         //   return View();
                        }
                           
                        }
                        else
                        {
                            return RedirectToAction("Index", "Login", new { @errorId = 1 });
                        }
                    //}

                }
                catch (Exception ex)
                {
                    throw ex;
                    //return RedirectToAction("Index", "Login", new { @errorId = 1 });
                }
            }
            else
            {
                return RedirectToAction("Index", "Login", new { @errorId = 1 });
            }
            //return View();
        }
        private string GenerateUL(DataRow[] menu, DataTable table, StringBuilder sb)
        {
            if (menu.Length > 0)
            {
                foreach (DataRow dr in menu)
                {
                    string url = dr["Url"].ToString();
                    string menuText = dr["Name"].ToString();
                    string icon = dr["CssClass"].ToString();
                    string active = "sub-parent";
                    if (url != "#")
                    {
                        string line = String.Format(@"<li><a href=""{0}""><i class=""{2}""></i> <span>{1}</span></a></li>", url, menuText, icon);
                        sb.Append(line);
                    }
                    string pid = dr["Id"].ToString();
                    string parentId = dr["ParentId"].ToString();
                    DataRow[] subMenu = table.Select(String.Format("ParentId = '{0}'", pid));
                    if (subMenu.Length > 0 && !pid.Equals(parentId))
                    {

                        string line = String.Format(@"<li class=""{2}""><a class=""dropdown-toggle"" href=""#""><i class=""{0}""></i> <span>{1}</span><b class=""arrow fa fa-angle-down""></b></a><ul class=""submenu"">", icon, menuText, active);
                        var subMenuBuilder = new StringBuilder();
                        sb.AppendLine(line);
                        sb.Append(GenerateUL(subMenu, table, subMenuBuilder));
                        sb.Append("</ul></li>");
                    }
                }
            }
            return sb.ToString();
        }
        public DataSet ToDataSet<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dataTable);
            return ds;
        }
        
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            //return RedirectToAction("Index", "Home");
            return RedirectToAction("Index", "Login");
        }
        
    }
}
