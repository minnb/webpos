﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebPOS.Dto;
using WebPOS.Models;
using WebPOS.Option;
using System.Data.SqlClient;
using System.Collections;
using System.Data;
using Microsoft.Extensions.Caching.Memory;
using WebPOS.Models.ViewModels;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WebPOS.Utils;
using WebPOS.Service;
using Microsoft.AspNetCore.Http;
using System.Xml;
using Formatting = Newtonsoft.Json.Formatting;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebPOS.Controllers
{
    
    public class OfferController : Controller
    {
        //private readonly ADO_NET _dbcon = new ADO_NET();
        private CentralDbContext db = new CentralDbContext();
        [Authorize]
        public IActionResult Index()
        {
            IEnumerable<SelectListItem> items = db.Store.Select(c => new SelectListItem
            {
                Value = c.No,
                Text = c.No + " + " + c.Name

            }); ;
            ViewBag.StoreNo = items;
            return View();
        }
        [Authorize]
        public async Task<IActionResult> LoadData(string StoreNo, string OfferNo, string ItemNo, string BarCode)
        {
            string BaseUrl = Startup.GetBaseUrl();
            string Token = Startup.GetTokenWebApi();
            string storeno = "";
            string offerno = "";
            string barcode = "";
            string itemno = "";
            //DataSet dataSet = new DataSet("TransRaw");
            // Hashtable param = new Hashtable();
            if (StoreNo != null)
            {
                storeno = "&_storeNo=" + StoreNo;
            }
            if (OfferNo != null)
            {
                offerno = "&_offerNo=" + OfferNo;
            }
            if (ItemNo != null)
            {
                itemno = "&_itemNo=" + ItemNo;
            }
            if (BarCode != null)
            {
                barcode = "&_barcode=" + BarCode;
            }
            
            string baseURL = BaseUrl + $"v1/WebApi/GetListOffer?_token=" + Token + storeno + offerno + itemno + barcode ;
            try
            {
                //Now we will have our using directives which would have a HttpClient 
                using (HttpClient client = new HttpClient())
                {
                    //Now get your response from the client from get request to baseurl.
                    //Use the await keyword since the get request is asynchronous, and want it run before next asychronous operation.
                    using (HttpResponseMessage res = await client.GetAsync(baseURL))
                    {
                        //Now we will retrieve content from our response, which would be HttpContent, retrieve from the response Content property.
                        using (HttpContent content = res.Content)
                        {
                            //Retrieve the data from the content of the response, have the await keyword since it is asynchronous.
                            var dataObj = await content.ReadAsStringAsync();
                            if (dataObj == "null")
                            {
                                return Json(new { draw = 0, recordsTotal = 0, recordsFiltered = 0, data = "" });
                            }
                            var array = JArray.Parse(dataObj);
                            var data = array.Select(emp => emp).ToList();

                            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                            // Skiping number of Rows count  
                            var start = Request.Form["start"].FirstOrDefault();
                            // Paging Length 10,20  
                            var length = Request.Form["length"].FirstOrDefault();
                            // Sort Column Name  
                            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                            // Sort Column Direction ( asc ,desc)  
                            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                            // Search Value from (Search box)  
                            // var searchValue = Request.Form["search[value]"].FirstOrDefault();

                            //Paging Size (10,20,50,100)  
                            int pageSize = length != null ? Convert.ToInt32(length) : 0;
                            int skip = start != null ? Convert.ToInt32(start) : 0;
                            int recordsTotal = 0;

                            //total number of rows count   
                            recordsTotal = data.Count();
                            //Paging   
                            var data2 = data.Skip(skip).Take(pageSize).ToList();

                            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data2 });
                            //return Json(data);

                        }


                    }
                }
            }
            catch
            {
                throw;
            }



        }

        [Authorize]
        [HttpPost]
        public IActionResult PostListTransRawAsync(string StoreNo, string ItemNo, string Barcode, string Description)
        {
            return View();
        }
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> ViewDetail(string id)
        {
            string BaseUrl = Startup.GetBaseUrl();
            string Token = Startup.GetTokenWebApi();
            ViewBag.sale = db.OfferHeader.Find(id);
            string offerno = "&_offerNo=" + id;
            string baseURL = BaseUrl + $"v1/WebApi/GetOfferDetail?_token=" + Token + offerno;

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //Now get your response from the client from get request to baseurl.
                    //Use the await keyword since the get request is asynchronous, and want it run before next asychronous operation.
                    using (HttpResponseMessage res = await client.GetAsync(baseURL))
                    {
                        //Now we will retrieve content from our response, which would be HttpContent, retrieve from the response Content property.
                        using (HttpContent content = res.Content)
                        {
                            //Retrieve the data from the content of the response, have the await keyword since it is asynchronous.
                            var dataObj = await content.ReadAsStringAsync();

                            var list = JObject.Parse(dataObj)["offerSite"].Select(el => el).ToList();
                            ViewBag.product = list;
                            var list2 = JObject.Parse(dataObj)["offerBuy"].Select(el => el).ToList();
                            ViewBag.product2 = list2;
                            var list3 = JObject.Parse(dataObj)["offerGet"].Select(el => el).ToList();
                            ViewBag.product3 = list3;
                            var list4 = JObject.Parse(dataObj)["offerBenefits"].Select(el => el).ToList();
                            ViewBag.product4 = list4;

                            return View("ViewDetail");

                        }


                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}